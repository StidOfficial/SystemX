#include <SystemX/Utils.hpp>

#include <cmath>
#include <sstream>
#include <iterator>
#include <openssl/sha.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>
#include <openssl/evp.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

namespace SystemX
{
	std::vector<std::string> Utils::Split(const std::string &text, const std::string &regex, const size_t limit)
	{
		std::vector<std::string> list;

		size_t last_pos = 0, pos = 0;
		while(true)
		{
			pos = text.find(regex, last_pos);
			if(pos == std::string::npos || (limit > 0 && list.size() + 1 > limit))
				break;

			list.push_back(std::string(text.begin() + last_pos, text.begin() + pos));
			last_pos = pos + regex.length();
		}

		list.push_back(std::string(text.begin() + last_pos, text.end()));

		return list;
	}

	std::string Utils::Join(const std::vector<std::string> array, std::string delimiter)
	{
		std::ostringstream stream;
		std::copy(array.begin(), array.end(), std::ostream_iterator<std::string>(stream, delimiter.c_str()));

		return stream.str();
	}

	std::string Utils::Trim(const std::string &text)
	{
		std::string output = text;

		int pos = -1;
		for(int i = 0; i < static_cast<int>(output.length()); i++)
			if(output[i] == ' ')
				pos = i + 1;
			else
				break;

		if(pos < 0)
			return output;

		output.erase(output.begin(), output.begin() + pos);
		return output;
	}

	std::string Utils::Fill(const std::string &character, const int number)
	{
		std::string text;
		for(int i = 0; i < number; i++)
			text += character;

		return text;
	}

	std::string Utils::TableFill(const std::string &text, const int size)
	{
		return text + Fill("\t", std::round(((size / 8) - (text.length() / 8)) + 1));
	}

	std::string Utils::LastError()
	{
		return std::string(std::strerror(errno));
	}

	std::string Utils::SHA1(const std::string str)
	{
		const unsigned char* uStr = reinterpret_cast<const unsigned char*>(str.c_str());
		unsigned char hash[SHA_DIGEST_LENGTH];

		::SHA1(uStr, str.size(), hash);

		return std::string(reinterpret_cast<char*>(hash), SHA_DIGEST_LENGTH);
	}

	std::string Utils::Base64Encode(const std::string str)
	{
		BIO *bio, *b64;
		BUF_MEM *buffer;

		b64 = BIO_new(BIO_f_base64());
		bio = BIO_new(BIO_s_mem());
		bio = BIO_push(b64, bio);

		BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
		BIO_write(bio, str.c_str(), str.size());
		BIO_flush(bio);

		BIO_get_mem_ptr(bio, std::addressof(buffer));
		BIO_set_close(bio, BIO_NOCLOSE);

		std::string result(buffer->data, buffer->length);

		free(buffer);

		BIO_free_all(bio);

		return result;
	}

	void* Utils::MemChunk(const off_t offset, const size_t length)
	{
		size_t pagesize = getpagesize();
		off_t page_base = (offset / pagesize) * pagesize;
		off_t page_offset = offset - page_base;

		int fd = open("/dev/mem", O_SYNC);
		void *mem = mmap(NULL, page_offset + length, PROT_READ, MAP_SHARED, fd, page_base);
		if(mem == MAP_FAILED)
			return nullptr;

		return (((uint8_t*)mem) + page_offset);
	}
}
