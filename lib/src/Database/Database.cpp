#include <SystemX/Database/Database.hpp>

#include <stdexcept>

namespace SystemX
{
	namespace Database
	{
		Database::Database(Driver driver)
			: m_driver(driver)
		{
		}

		Driver Database::GetDriver()
		{
			return m_driver;
		}

		std::string Database::LastError()
		{
			switch(GetDriver())
			{
				case Driver::SQLite:
				{
					return std::string(sqlite3_errmsg(m_sqlite));
				}
			}

			return std::string();
		}

		void Database::Open(std::string path)
		{
			switch(GetDriver())
			{
				case Driver::SQLite:
				{
					if(sqlite3_open_v2(path.c_str(), &m_sqlite, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, nullptr) != SQLITE_OK)
						throw std::runtime_error("Open() : " + LastError());

					break;
				}
			}
		}

		DatabaseStatement *Database::Prepare(std::string statement)
		{
			switch(GetDriver())
			{
				case Driver::SQLite:
				{
					sqlite3_stmt *sqlite_statement;
					if(sqlite3_prepare(m_sqlite, statement.c_str(), -1, &sqlite_statement, nullptr) != SQLITE_OK)
						throw std::runtime_error("Prepare() : " + LastError());

					return new DatabaseStatement(this, sqlite_statement);
				}
			}

			return nullptr;
		}

		void Database::Query(std::string /*statement*/)
		{
			// Not used at now
		}

		int Database::Exec(std::string statement)
		{
			switch(GetDriver())
			{
				case Driver::SQLite:
				{
					if(sqlite3_exec(m_sqlite, statement.c_str(), nullptr, nullptr, nullptr) != SQLITE_OK)
						throw std::runtime_error("SQL Error : " + LastError());
					return sqlite3_changes(m_sqlite);
				}
			}

			return 0;
		}

		uint64_t Database::LastInsertRowID()
		{
			switch(GetDriver())
			{
				case Driver::SQLite:
				{
					return sqlite3_last_insert_rowid(m_sqlite);
				}
			}

			return -1;
		}

		void Database::Close()
		{
			switch(GetDriver())
			{
				case Driver::SQLite:
				{
					sqlite3_close(m_sqlite);
					break;
				}
			}
		}

		Database::~Database()
		{
		}
	}
}
