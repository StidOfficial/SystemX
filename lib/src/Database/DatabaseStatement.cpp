#include <SystemX/Database/DatabaseStatement.hpp>

namespace SystemX
{
	namespace Database
	{
		DatabaseStatement::DatabaseStatement()
			: m_database(nullptr), m_sqlite_statement(nullptr)
		{
		}

		DatabaseStatement::DatabaseStatement(Database *database, sqlite3_stmt *statement)
			: m_database(database), m_sqlite_statement(statement)
		{
			switch(m_database->GetDriver())
			{
				case Driver::SQLite:
				{
					int paramsCount = sqlite3_bind_parameter_count(m_sqlite_statement);
					for(int i = 1; i <= paramsCount; i++)
					{
						const std::string paramName = std::string(sqlite3_bind_parameter_name(m_sqlite_statement, i));
						m_params[paramName] = i;
					}

					int columnCount = sqlite3_column_count(m_sqlite_statement);
					for(int i = 0; i < columnCount; i++)
					{
						const std::string columnName = std::string(sqlite3_column_name(m_sqlite_statement, i));
						m_columns[columnName] = i;
					}
					break;
				}
			}
		}

		void DatabaseStatement::Bind(std::string name, int value)
		{
			Bind(GetParamByName(name), value);
		}

		void DatabaseStatement::Bind(int index, int value)
		{
			switch(m_database->GetDriver())
			{
				case Driver::SQLite:
				{
					if(sqlite3_bind_int(m_sqlite_statement, index, value) != SQLITE_OK)
						throw std::runtime_error("Bind() : " + m_database->LastError());

					break;
				}
			}
		}

		void DatabaseStatement::Bind(std::string name, std::string value)
		{
			Bind(GetParamByName(name), value);
		}

		void DatabaseStatement::Bind(int index, std::string value)
		{
			switch(m_database->GetDriver())
			{
				case Driver::SQLite:
				{
					if(sqlite3_bind_text(m_sqlite_statement, index, value.c_str(), -1, SQLITE_TRANSIENT) != SQLITE_OK)
						throw std::runtime_error("SQL Error : " + m_database->LastError());
					break;
				}
			}
		}

		int DatabaseStatement::GetInt(std::string name)
		{
			switch(m_database->GetDriver())
			{
				case Driver::SQLite:
				{
					return sqlite3_column_int(m_sqlite_statement, GetColumnByName(name));
				}
			}

			return -1;
		}

		std::string DatabaseStatement::GetString(std::string name)
		{
			switch(m_database->GetDriver())
			{
				case Driver::SQLite:
				{
					const unsigned char* value = sqlite3_column_text(m_sqlite_statement, GetColumnByName(name));
					return std::string((char*)value);
				}
			}

			return std::string();
		}

		void DatabaseStatement::Execute()
		{
			First();
		}

		bool DatabaseStatement::First()
		{
			return Next();
		}

		bool DatabaseStatement::Next()
		{
			switch(m_database->GetDriver())
			{
				case Driver::SQLite:
				{
					return sqlite3_step(m_sqlite_statement) == SQLITE_ROW;
				}
			}

			return false;
		}

		int DatabaseStatement::GetColumnByName(std::string name)
		{
			for(const auto& column : m_columns)
				if(column.first == name)
					return column.second;

			return -1;
		}

		int DatabaseStatement::GetParamByName(std::string name)
		{
			for(const auto& param : m_params)
				if(param.first == name)
					return param.second;

			return -1;
		}

		std::string DatabaseStatement::SQL()
		{
			std::string sql;

			char *sql_buffer = sqlite3_expanded_sql(m_sqlite_statement);
			sql = std::string(sql_buffer);
			sqlite3_free(sql_buffer);

			return sql;
		}

		DatabaseStatement::~DatabaseStatement()
		{
			switch(m_database->GetDriver())
			{
				case Driver::SQLite:
				{
					sqlite3_finalize(m_sqlite_statement);
					break;
				}
			}
		}
	}
}
