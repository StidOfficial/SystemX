#include <SystemX/Network/InetAddress.hpp>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdexcept>
#include <mutex>

namespace SystemX
{
	namespace Network
	{
		InetAddress::InetAddress()
			: m_socklen(0)
		{
		}

		InetAddress::InetAddress(struct sockaddr addr, socklen_t len)
			: m_sockaddr(addr), m_socklen(len)
		{
		}

		InetAddress::InetAddress(IPAddress address, int port)
			: m_socklen(0)
		{
			switch(address)
			{
				case IPAddress::IPv4Any:
				{
					struct sockaddr_in *addr_in = reinterpret_cast<struct sockaddr_in*>(&m_sockaddr);

					addr_in->sin_family = (sa_family_t)AddressFamily::IPv4;
					addr_in->sin_addr.s_addr = INADDR_ANY;
					addr_in->sin_port = htons(port);

					m_socklen = sizeof(struct sockaddr_in);
					break;
				}
				case IPAddress::IPv6Any:
				{
					struct sockaddr_in6 *addr_in6 = reinterpret_cast<struct sockaddr_in6*>(&m_sockaddr);

					addr_in6->sin6_family = (sa_family_t)AddressFamily::IPv6;
					addr_in6->sin6_addr = IN6ADDR_ANY_INIT;
					addr_in6->sin6_port = htons(port);

					m_socklen = sizeof(struct sockaddr_in6);
					break;
				}
			}
		}

		InetAddress::InetAddress(sa_family_t family)
		{
			m_sockaddr.sa_family = family;

			switch(m_sockaddr.sa_family)
			{
				case (int)ProtocolFamily::IPv4:
					m_socklen = sizeof(struct sockaddr_in);
					break;
				case (int)ProtocolFamily::IPv6:
					m_socklen = sizeof(struct sockaddr_in6);
					break;
				default:
					throw std::invalid_argument("Family not supported");
			}
		}

		InetAddress::InetAddress(AddressFamily family)
			: InetAddress(static_cast<sa_family_t>(family))
		{
		}

		/*InetAddress::InetAddress(sa_family_t family, char *address)
			: InetAddress(family)
		{
			switch((AddressFamily)m_sockaddr.sa_family)
			{
				case AddressFamily::IPv4:
				{
					struct sockaddr_in *addr_in = reinterpret_cast<struct sockaddr_in*>(&m_sockaddr);
					addr_in->sin_addr = reinterpret_cast<struct in_addr&>(address);
					break;
				}
				case AddressFamily::IPv6:
				{
					struct sockaddr_in6 *addr_in6 = reinterpret_cast<struct sockaddr_in6*>(&m_sockaddr);
					addr_in6->sin6_addr = reinterpret_cast<struct in6_addr&>(address);
					break;
				}
				default:
					throw std::invalid_argument("Family not supported");
			}
		}*/

		InetAddress::InetAddress(AddressFamily family, std::string address, uint16_t port)
			: InetAddress(family)
		{
			SetAddress(address);
			SetPort(port);
		}

		AddressFamily InetAddress::GetAddressFamily()
		{
			return static_cast<AddressFamily>(m_sockaddr.sa_family);
		}

		ProtocolFamily InetAddress::GetProtocolFamily()
		{
			return static_cast<ProtocolFamily>(m_sockaddr.sa_family);
		}

		bool InetAddress::IsIPv4()
		{
			return GetAddressFamily() == AddressFamily::IPv4;
		}

		bool InetAddress::IsIPv6()
		{
			return GetAddressFamily() == AddressFamily::IPv6;
		}

		void InetAddress::SetPort(uint16_t port)
		{
			switch(GetAddressFamily())
			{
				case AddressFamily::IPv4:
				{
					struct sockaddr_in *addr_in = reinterpret_cast<struct sockaddr_in*>(&m_sockaddr);
					addr_in->sin_port = htons(port);
					break;
				}
				case AddressFamily::IPv6:
				{
					struct sockaddr_in6 *addr_in = reinterpret_cast<struct sockaddr_in6*>(&m_sockaddr);
					addr_in->sin6_port = htons(port);
					break;
				}
				default:
					throw std::invalid_argument("Family not supported");
			}
		}

		uint16_t InetAddress::GetPort()
		{
			struct sockaddr_in *addr_in = reinterpret_cast<struct sockaddr_in*>(&m_sockaddr);
			struct sockaddr_in6 *addr_in6 = reinterpret_cast<struct sockaddr_in6*>(&m_sockaddr);

			switch(GetAddressFamily())
			{
				case AddressFamily::IPv4:
					return ntohs(addr_in->sin_port);
				case AddressFamily::IPv6:
					return ntohs(addr_in6->sin6_port);
				default:
					throw std::invalid_argument("Family not supported");
			}
		}

		void InetAddress::SetAddress(std::string address)
		{
			std::vector<InetAddress> address_info = GetAddressInfo(address, GetAddressFamily());
			if(address_info.size() <= 0)
				throw std::invalid_argument("No host found !");

			InetAddress inet_addr = address_info.at(0);
			struct sockaddr *addr = inet_addr.GetSocketAddress();
			switch(GetAddressFamily())
			{
				case AddressFamily::IPv4:
				{
					struct sockaddr_in *addr_in = reinterpret_cast<struct sockaddr_in*>(&m_sockaddr);
					struct sockaddr_in *host_addr_in = reinterpret_cast<struct sockaddr_in*>(addr);
					addr_in->sin_family = host_addr_in->sin_family;
					addr_in->sin_addr = host_addr_in->sin_addr;

					m_socklen = sizeof(struct sockaddr_in);

					break;
				}
				case AddressFamily::IPv6:
				{
					struct sockaddr_in6 *addr_in6 = reinterpret_cast<struct sockaddr_in6*>(&m_sockaddr);
					struct sockaddr_in6 *host_addr_in6 = reinterpret_cast<struct sockaddr_in6*>(addr);
					addr_in6->sin6_family = host_addr_in6->sin6_family;
					addr_in6->sin6_addr = host_addr_in6->sin6_addr;

					m_socklen = sizeof(struct sockaddr_in6);

					break;
				}
				default:
					throw std::runtime_error("Family not supported");
			}
		}

		void InetAddress::SetAddress(struct in_addr address)
		{
			struct sockaddr_in *addr_in = reinterpret_cast<struct sockaddr_in*>(&m_sockaddr);
			addr_in->sin_family = AF_INET;
			addr_in->sin_addr = address;
		}

		void InetAddress::SetAddress(struct in6_addr address)
		{
			struct sockaddr_in6 *addr_in6 = reinterpret_cast<struct sockaddr_in6*>(&m_sockaddr);
			addr_in6->sin6_family = AF_INET6;
			addr_in6->sin6_addr = address;
		}

		std::string InetAddress::GetAddress()
		{
			std::string address;
			struct sockaddr_in *addr_in = reinterpret_cast<struct sockaddr_in*>(&m_sockaddr);
			struct sockaddr_in6 *addr_in6 = reinterpret_cast<struct sockaddr_in6*>(&m_sockaddr);

			switch(GetAddressFamily())
			{
				case AddressFamily::IPv4:
				{
					inet_ntop(addr_in->sin_family, &addr_in->sin_addr, const_cast<char*>(address.c_str()), INET_ADDRSTRLEN);
					break;
				}
				case AddressFamily::IPv6:
				{
					inet_ntop(addr_in6->sin6_family, &addr_in6->sin6_addr, const_cast<char*>(address.c_str()), INET6_ADDRSTRLEN);
					break;
				}
				default:
					throw std::invalid_argument("Family not supported");
			}

			return address;
		}

		std::string InetAddress::GetHostname()
		{
			std::string address;
			address.resize(NI_MAXHOST);

			getnameinfo(&m_sockaddr, m_socklen, &address[0], address.length(), nullptr, 0, NI_NAMEREQD);

			return address;
		}

		struct sockaddr *InetAddress::GetSocketAddress()
		{
			return &m_sockaddr;
		}

		socklen_t InetAddress::GetSocketAddressLength()
		{
			return m_socklen;
		}

		std::vector<InetAddress> InetAddress::GetHostByName(std::string hostname)
		{
			return GetAddressInfo(hostname);
		}

		std::mutex addrinfo;
		std::vector<InetAddress> InetAddress::GetAddressInfo(std::string hostname, AddressFamily family)
		{
			std::vector<InetAddress> address;

			struct addrinfo hints = {
				.ai_flags = AI_PASSIVE,
				.ai_family = static_cast<int>(family),
				.ai_socktype = SOCK_STREAM,
				.ai_protocol = 0,//IPPROTO_TCP,
				.ai_addrlen = 0,
				.ai_addr = nullptr,
				.ai_canonname = nullptr,
				.ai_next = nullptr
			};

			addrinfo.lock();

			struct addrinfo *res, *res_result;

			int result = getaddrinfo(hostname.c_str(), nullptr, &hints, &res_result);
			if(result < 0)
				throw std::runtime_error("getaddrinfo failed: " + std::string(gai_strerror(result)));

			addrinfo.unlock();

			for (res = res_result; res != nullptr; res = res->ai_next)
				address.push_back(InetAddress(*res->ai_addr, res->ai_addrlen));

			freeaddrinfo(res_result);

			return address;
		}
	}
}
