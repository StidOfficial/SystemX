#include <SystemX/Network/HTTP/Controller.hpp>

#include <SystemX/Logger.hpp>
#include <SystemX/Network/HTTP/HTTP.hpp>
#include <SystemX/Utils.hpp>
#include <SystemX/Network/HTTP/WebSocketMessage.hpp>
#include <SystemX/MimeTypes.hpp>

#include <experimental/filesystem>
#include <fstream>
#include <ctime>
#include <sstream>
#include <iomanip>
#include <chrono>

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			Controller::Controller()
			{
			}

			void Controller::Get(Client *client, Request */*request*/, Response *response)
			{
				response->SetStatusCode(405);
				client->Send(response);
			}

			void Controller::Head(Client *client, Request */*request*/, Response *response)
			{
				response->SetStatusCode(405);
				client->Send(response);
			}

			void Controller::Post(Client *client, Request */*request*/, Response *response)
			{
				response->SetStatusCode(405);
				client->Send(response);
			}

			void Controller::Put(Client *client, Request */*request*/, Response *response)
			{
				response->SetStatusCode(405);
				client->Send(response);
			}

			void Controller::Delete(Client *client, Request */*request*/, Response *response)
			{
				response->SetStatusCode(405);
				client->Send(response);
			}

			void Controller::Connect(Client *client, Request */*request*/, Response *response)
			{
				response->SetStatusCode(405);
				client->Send(response);
			}

			void Controller::Options(Client *client, Request */*request*/, Response *response)
			{
				response->SetStatusCode(405);
				client->Send(response);
			}

			void Controller::Trace(Client *client, Request */*request*/, Response *response)
			{
				response->SetStatusCode(405);
				client->Send(response);
			}

			void Controller::Patch(Client *client, Request */*request*/, Response *response)
			{
				response->SetStatusCode(405);
				client->Send(response);
			}

			void Controller::OnRequest(Client *client, Request *request, Response *response)
			{
				if(request->GetMethod() == Method::GET)
					Get(client, request, response);
				else if(request->GetMethod() == Method::HEAD)
					Head(client, request, response);
				else if(request->GetMethod() == Method::POST)
					Post(client, request, response);
				else if(request->GetMethod() == Method::PUT)
					Put(client, request, response);
				else if(request->GetMethod() == Method::DELETE)
					Delete(client, request, response);
				else if(request->GetMethod() == Method::CONNECT)
					Connect(client, request, response);
				else if(request->GetMethod() == Method::OPTIONS)
					Options(client, request, response);
				else if(request->GetMethod() == Method::TRACE)
					Trace(client, request, response);
				else if(request->GetMethod() == Method::PATCH)
					Patch(client, request, response);
				else
				{
					response->SetStatusCode(400);
					client->Send(response);
				}
			}

			void Controller::OnRequestWebSocket(Client *client, Request *request, Response *response)
			{
				response->SetStatusCode(101);
				response->Headers()->Add("Upgrade", "websocket");
				response->Headers()->Add("Connection", "Upgrade");

				std::string key = request->Headers()->Get("Sec-WebSocket-Key");
				std::string acceptKey = key + HTTP_WEBSOCKET_MAGIC;

				response->Headers()->Add("Sec-WebSocket-Accept", Utils::Base64Encode(Utils::SHA1(acceptKey)));

				client->Send(response);

				std::vector<char> buffer(2048);
				while(client->IsConnected())
				{
					WebSocketMessage message(client);

					if(message.GetOpCode() == WebSocketMessage::OpCode::Close)
						client->Shutdown();
					else
					{
						std::vector<unsigned char> payload = message.GetPayload();
						SystemX::Logger::Debug("Size : %d", payload.size());
						SystemX::Logger::Debug("OnMessage : %s", std::string(payload.begin(), payload.end()).c_str());
					}
				}
			}

			void Controller::OnRequestFile(Client *client, Request *request, Response *response)
			{
				std::experimental::filesystem::path filePath = "/home/stidofficial/Documents/SystemX/www" + request->Uri().Path();
				std::string contentType = MimeTypes::find(filePath);

				if(request->Uri().Path().find_first_of("/") != 0)
				{
					response->SetStatusCode(400);
					client->Send(response);
					client->Send(response->StatusMessage());

					return;
				}

				if(std::experimental::filesystem::exists(filePath))
				{
					auto chonoFileLastWriteTime = std::experimental::filesystem::last_write_time(filePath);
					std::time_t timeFileLastWriteTime = decltype(chonoFileLastWriteTime)::clock::to_time_t(chonoFileLastWriteTime);

					std::string strFileLastWriteTime = TimeToString(timeFileLastWriteTime);

					/*auto chronoNow = std::chrono::system_clock::now();
					std::chrono::system_clock::time_point chronoExpires = chronoNow + std::chrono::hours(16 * 24);
					std::time_t timeExpires = decltype(chronoExpires)::clock::to_time_t(chronoExpires);

					std::stringstream ssExpires;
					ssExpires << std::put_time(std::gmtime(&timeExpires), HTTP_DATE_FORMAT);
					std::string expires = ssExpires.str();*/

					std::time_t timeIfModifiedSince = StringToTime(request->Headers()->Get("If-Modified-Since"));

					if(timeIfModifiedSince > 0)
					{
						if(timeFileLastWriteTime == timeIfModifiedSince)
						{
							response->SetStatusCode(304);
							response->Headers()->Add("X-Content-Type-Options", "nosniff");
							client->Send(response);

							return;
						}
					}

					if(std::experimental::filesystem::is_regular_file(filePath))
					{
						try
						{
							std::uintmax_t fileSize = std::experimental::filesystem::file_size(filePath);

							response->SetStatusCode(200);
							response->Headers()->Add("Content-Length", fileSize);
							if(!contentType.empty())
								response->Headers()->Add("Content-Type", contentType);
							//response->Headers()->Add("X-Content-Type-Options", "nosniff");
							//response.Headers()->Add("Expires", expires);
							response->Headers()->Add("Last-Modified", strFileLastWriteTime);

							std::ifstream file(filePath);
							if(file.is_open())
							{
								client->Send(response);

								std::vector<char> buffer(1024);
								do
								{
									file.read(&buffer[0], buffer.size());
									if(file.gcount() > 0)
										client->Send(buffer, file.gcount());
								}
								while(file.tellg() > 0 && file.tellg() < static_cast<std::streamsize>(fileSize));

								file.close();
							}
							else
							{
								response->SetStatusCode(500);
								client->Send(response);
							}
						}
						catch(std::experimental::filesystem::filesystem_error& e)
						{
							SystemX::Logger::Error("Error OnRequestFile : %s", e.what());

							response->SetStatusCode(500);
							client->Send(response);
						}
					}
					else if(std::experimental::filesystem::is_directory(filePath))
					{
						response->SetStatusCode(200);
						//response.Headers()->Add("Expires", expires);
						response->Headers()->Add("Last-Modified", strFileLastWriteTime);

						client->Send(response);
						client->Send("<html>\r\n");
						client->Send("<head><title>Index Of " + request->Uri().Path() + "</title></head>\r\n");
						client->Send("<body><h1>Index Of " + request->Uri().Path() + "</h1>\r\n");
						client->Send("<pre>\r\n");

						client->Send("<a href=\"../\">../</a>\r\n");

						for(const auto& entry : std::experimental::filesystem::directory_iterator(filePath))
						{
							bool is_directory = std::experimental::filesystem::is_directory(entry.status());
							std::string entryPath = entry.path().filename().string();

							if(is_directory)
								entryPath += "/";

							client->Send("<a href=\"" + entryPath + "\">" + entryPath + "</a>\r\n");
						}

						client->Send("</pre>\r\n");
						client->Send("<html>");
					}
				}
				else
				{
					response->SetStatusCode(404);
					client->Send(response);
					client->Send(response->StatusMessage());
				}
			}

			Controller::~Controller()
			{
			}
		}
	}
}
