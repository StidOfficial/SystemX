#include <SystemX/Network/HTTP/Cookies.hpp>

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			Cookies::Cookies()
			{
			}

			void Cookies::Set(Cookie *cookie)
			{
				std::vector<Cookie*>::iterator itCookie = m_cookies.end();
				for(auto it = m_cookies.begin(); it != m_cookies.end(); it++)
				{
					Cookie *cookieItem = *it;
					if(cookieItem->Name() == cookie->Name() && cookieItem->Domain() == cookie->Domain() && cookieItem->Path() == cookie->Path())
					{
						itCookie = it;
						break;
					}
				}

				if(itCookie != m_cookies.end())
					m_cookies.erase(itCookie);

				m_cookies.push_back(cookie);
			}

			void Cookies::Remove(std::string domain, std::string name)
			{
				for(auto it = m_cookies.begin(); it != m_cookies.end(); it++)
				{
					Cookie *cookie = *it;
					if(cookie->Domain() == domain && cookie->Name() == name)
						m_cookies.erase(it);
				}
			}

			Cookie *Cookies::Get(std::string domain, std::string name, std::string path)
			{
				for(auto &cookie : Get(domain, path))
					if(cookie->Name() == name)
						return cookie;

				return nullptr;
			}

			std::vector<Cookie*> Cookies::Get(std::string domain, std::string path)
			{
				std::vector<Cookie*> cookies;
				for(auto &cookie : m_cookies)
					if(cookie->Domain() == domain && path.find(cookie->Path(), 0) == 0)
						cookies.push_back(cookie);

				return cookies;
			}

			std::string Cookies::ToString(std::string domain, std::string path)
			{
				std::vector<Cookie*> cookies = Get(domain, path);

				std::string str;
				for(auto it = cookies.begin(); it < cookies.end(); it++)
				{
					Cookie *cookie = *it;
					str.append(cookie->Name() + "=" + cookie->Value());
					str.append(((it != cookies.end()) ? "; " : ""));
				}

				return str;
			}

			std::size_t Cookies::Size()
			{
				return m_cookies.size();
			}

			Cookies::~Cookies()
			{
				for(auto &cookie : m_cookies)
					delete cookie;
			}
		}
	}
}
