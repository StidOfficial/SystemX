#include <SystemX/Network/HTTP/Request.hpp>

#include <SystemX/Network/HTTP/Client.hpp>
#include <SystemX/Utils.hpp>

#include <vector>
#include <exception>

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			Request::Request(Client *client)
				: m_parsed(true)
			{
				const std::string httpNewLine = "\r\n";
				const std::string httpHeaderEnd = "\r\n\r\n";

				std::vector<char> headerBuffer(8000);
				bool is_valid_header = false;
				for(size_t i = 0; i < headerBuffer.size(); i++)
				{
					client->Recv(&headerBuffer[i], sizeof(char));

					if(i >= httpHeaderEnd.length())
						if(std::string(headerBuffer.begin() + ((i + 1) - httpHeaderEnd.length()), headerBuffer.begin() + (i + 1)) == httpHeaderEnd)
						{
							is_valid_header = true;
							headerBuffer.resize(i + 1);
							break;
						}
				}

				if(!is_valid_header)
					throw std::runtime_error("Out of range header with 8k bytes limit");

				std::string header(headerBuffer.begin(), headerBuffer.end());
				auto pos = header.find(httpNewLine);
				std::string request(header.begin(), header.begin() + pos);

				std::vector<std::string> requestParams = SystemX::Utils::Split(request, " ", 2);
				if(requestParams.size() != 3)
					throw std::runtime_error("Bad request");

				m_method = StringTo(requestParams[0]);
				m_uri = URI(requestParams[1]);
				m_protocolVersion = requestParams[2];

				header.erase(header.begin(), header.begin() + pos + httpNewLine.length());
				header.erase(header.end() - httpHeaderEnd.length(), header.end());

				m_headers = HTTP::Headers::Parse(header);
			}

			Request::Request(Method method, Network::URI uri, HTTP::Headers headers)
				: Request(method, uri, headers, nullptr)
			{
			}

			Request::Request(Method method, Network::URI uri, HTTP::Headers headers, Cookies *cookies)
				: m_parsed(false), m_method(method), m_uri(uri), m_protocolVersion("HTTP/1.1")
			{
				m_headers = &headers;

				m_headers->Set("Host", uri.Host());
				m_headers->Set("User-Agent", "SystemX-Agent");
				if(cookies)
					m_headers->Set("Cookie", cookies->ToString(uri.Host(), uri.Path()));
			}

			void Request::SetProtocolVersion(std::string protocolVersion)
			{
				m_protocolVersion = protocolVersion;
			}

			std::string Request::GetProtocolVersion()
			{
				return m_protocolVersion;
			}

			void Request::SetMethod(Method method)
			{
				m_method = method;
			}

			Method Request::GetMethod()
			{
				return m_method;
			}

			Network::URI Request::Uri()
			{
				return m_uri;
			}

			HTTP::Headers* Request::Headers()
			{
				return m_headers;
			}

			std::string Request::ToString()
			{
				return MethodTo(m_method) + " " + m_uri.AbsolutePath() + " " + m_protocolVersion + "\r\n" + m_headers->ToString() + "\r\n";
			}

			Method Request::StringTo(std::string method)
			{
				if(method == "GET")
					return Method::GET;
				else if(method == "HEAD")
					return Method::HEAD;
				else if(method == "POST")
					return Method::POST;
				else if(method == "PUT")
					return Method::PUT;
				else if(method == "DELETE")
					return Method::DELETE;
				else if(method == "TRACE")
					return Method::TRACE;
				else if(method == "OPTIONS")
					return Method::OPTIONS;
				else if(method == "CONNECT")
					return Method::CONNECT;
				else if(method == "PATCH")
					return Method::PATCH;

				throw std::invalid_argument("Unknown method");
			}

			std::string Request::MethodTo(Method method)
			{
				switch(method)
				{
					case Method::GET:
						return "GET";
					case Method::HEAD:
						return "HEAD";
					case Method::POST:
						return "POST";
					case Method::PUT:
						return "PUT";
					case Method::DELETE:
						return "DELETE";
					case Method::TRACE:
						return "TRACE";
					case Method::OPTIONS:
						return "OPTIONS";
					case Method::CONNECT:
						return "CONNECT";
					case Method::PATCH:
						return "PATCH";
				}

				return "UNKNOWN";
			}

			Request::~Request()
			{
				if(m_parsed)
					delete m_headers;
			}
		}
	}
}
