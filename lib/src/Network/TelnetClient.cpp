#include <SystemX/Network/TelnetClient.hpp>

namespace SystemX
{
	namespace Network
	{
		TelnetClient::TelnetClient(int socket, InetAddress *address, SocketServer<Socket> *server)
			: TCPClient(socket, address, server)
		{
		}

		void TelnetClient::SetOption(TelnetCommand command, TelnetOption option)
		{
			m_options_buffer.push_back((char)TelnetCommand::IAC);
			m_options_buffer.push_back((char)command);
			m_options_buffer.push_back((char)option);
		}

		void TelnetClient::SendOption()
		{
			Send(&m_options_buffer[0], m_options_buffer.size());
			m_options_buffer.clear();
		}

		void TelnetClient::SendCommand(TelnetCommand command)
		{
			std::vector<char> command_buffer {(char)TelnetCommand::IAC, (char)command};
			Send(&command_buffer[0], command_buffer.size());
		}

		void TelnetClient::ReadIAC()
		{
			char command[3];
			Recv(&command, sizeof(command));
		}
	}
}
