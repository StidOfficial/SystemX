#include <SystemX/Equipment/Equipment.hpp>

namespace SystemX
{
	namespace Equipment
	{
		Equipment::Equipment(Hardware::Hardwares hardwares, std::vector<OperatingSystem::OperatingSystem*> operating_systems)
			: m_hardwares(hardwares), m_operating_systems(operating_systems),
				m_interfaces(std::unique_ptr<Network::Interfaces>(new Network::Interfaces(this))),
				m_drivers(std::unique_ptr<SystemX::Equipment::Drivers>(new SystemX::Equipment::Drivers(this)))
		{
		}

		Hardware::Hardwares Equipment::Hardwares()
		{
			return m_hardwares;
		}

		std::vector<OperatingSystem::OperatingSystem*> Equipment::OperatingSystems()
		{
			return m_operating_systems;
		}

		Network::Interfaces *Equipment::Interfaces()
		{
			return m_interfaces.get();
		}

		SystemX::Equipment::Drivers *Equipment::Drivers()
		{
			return m_drivers.get();
		}

		Equipment::~Equipment()
		{
		}
	}
}
