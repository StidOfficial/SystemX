#include <SystemX/Equipment/LocalEquipment.hpp>

#include <SystemX/Logger.hpp>
#include <SystemX/SMBIOS/SMBIOS.hpp>

namespace SystemX
{
	namespace Equipment
	{
		Hardware::Hardwares *LocalEquipment::m_hardwares = nullptr;
		OperatingSystem::OperatingSystems *LocalEquipment::m_operating_systems = nullptr;
		OperatingSystem::LocalOperatingSystem *LocalEquipment::m_localOperatingSystem = nullptr;

		void LocalEquipment::Initialize()
		{
			std::vector<Hardware::CPU> cpus;

			m_hardwares = new Hardware::Hardwares(cpus);
			m_operating_systems = new OperatingSystem::OperatingSystems();
			m_localOperatingSystem = new OperatingSystem::LocalOperatingSystem();

			SMBIOS::SMBIOS::Initialize();

			SMBIOS::BIOSInformation *biosInformation = SMBIOS::SMBIOS::GetBIOSInformation();
			SystemX::Logger::Debug("Vendor : %s", biosInformation->Vendor().c_str());
			SystemX::Logger::Debug("Version : %s", biosInformation->Version().c_str());
			SystemX::Logger::Debug("Starting adress segment : %#.4x", biosInformation->StartingAddressSegment());
			SystemX::Logger::Debug("Release date : %s", biosInformation->ReleaseDate().c_str());
			SystemX::Logger::Debug("ROM size : %d", biosInformation->ROMSize());
			SystemX::Logger::Debug("System BIOS major release : %d", biosInformation->SystemBIOSMajorRelease());
			SystemX::Logger::Debug("System BIOS minor release : %d", biosInformation->SystemBIOSMinorRelease());
			SystemX::Logger::Debug("Embedded controller firmware major release : %d", biosInformation->EmbeddedControllerFirmwareMajorRelease());
			SystemX::Logger::Debug("Embedded controller firmware minor release : %d", biosInformation->EmbeddedControllerFirmwareMinorRelease());
			SystemX::Logger::Debug("PCI is supported %d", biosInformation->PCISupported());
			SystemX::Logger::Debug("PCMCIA is supported %d", biosInformation->PCMCIASupported());
		}

		Hardware::Hardwares *LocalEquipment::Hardwares()
		{
			return m_hardwares;
		}

		OperatingSystem::OperatingSystems *LocalEquipment::OperatingSystems()
		{
			return m_operating_systems;
		}

		OperatingSystem::LocalOperatingSystem *LocalEquipment::CurrentOperatingSystem()
		{
			return m_localOperatingSystem;
		}

		void LocalEquipment::Destroy()
		{
			delete m_hardwares;
			delete m_operating_systems;
			delete m_localOperatingSystem;

			SMBIOS::SMBIOS::Destroy();
		}
	}
}
