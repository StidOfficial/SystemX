#include <SystemX/Equipment/Hardware/Hardware.hpp>

namespace SystemX
{
	namespace Equipment
	{
		namespace Hardware
		{
			Hardware::Hardware(Type type)
				: m_type(type)
			{
			}


			Hardware::Type Hardware::GetType()
			{
				return m_type;
			}
		}
	}
}
