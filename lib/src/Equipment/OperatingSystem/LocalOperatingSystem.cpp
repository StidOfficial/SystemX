#include <SystemX/Equipment/OperatingSystem/LocalOperatingSystem.hpp>

#include <SystemX/Utils.hpp>

#include <unistd.h>
#include <limits.h>

namespace SystemX
{
	namespace Equipment
	{
		namespace OperatingSystem
		{
			LocalOperatingSystem::LocalOperatingSystem()
			{
				char hostname[HOST_NAME_MAX];
				int result = gethostname(hostname, sizeof(hostname));
				if(result < 0)
					throw std::runtime_error("gethostname() : " + Utils::LastError());

				OperatingSystem::SetHostname(hostname);
			}

			void LocalOperatingSystem::SetHostname(std::string hostname)
			{
				int result = sethostname(hostname.c_str(), hostname.length());
				if(result < 0)
					throw std::runtime_error("sethostname() : " + Utils::LastError());

				OperatingSystem::SetHostname(hostname);
			}

			LocalOperatingSystem::~LocalOperatingSystem()
			{
			}
		}
	}
}
