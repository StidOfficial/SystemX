#include <SystemX/Equipment/Drivers.hpp>

#include <algorithm>

namespace SystemX
{
	namespace Equipment
	{
		Drivers::Drivers(Equipment *equipment)
			: m_equipment(equipment)
		{
		}

		bool Drivers::Exist(Driver *driver)
		{
			return std::find(m_drivers.begin(), m_drivers.end(), driver) != m_drivers.end();
		}

		void Drivers::Add(Driver *driver)
		{
			m_drivers.insert(m_drivers.end(), driver);
			driver->Add(m_equipment);
		}

		void Drivers::Remove(Driver *driver)
		{
			auto it = std::find(m_drivers.begin(), m_drivers.end(), driver);

			if(it != m_drivers.end())
				m_drivers.erase(it);
		}

		std::vector<Driver*>::iterator Drivers::begin()
		{
			return m_drivers.begin();
		}

		std::vector<Driver*>::iterator Drivers::end()
		{
			return m_drivers.end();
		}
	}
}
