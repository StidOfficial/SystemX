#include <SystemX/Equipment/Network/Interfaces.hpp>

namespace SystemX
{
	namespace Equipment
	{
		namespace Network
		{
			Interfaces::Interfaces(Equipment *equipment)
				: m_equipment(equipment)
			{
			}

			void Interfaces::Add(Interface *interface)
			{
				m_interfaces.push_back(interface);
			}

			Interface *Interfaces::Get(std::string name)
			{
				for(auto &interface : m_interfaces)
					if(interface->GetName() == name)
						return interface;

				return nullptr;
			}

			std::vector<Interface*>::iterator Interfaces::begin()
			{
				return m_interfaces.begin();
			}

			std::vector<Interface*>::iterator Interfaces::end()
			{
				return m_interfaces.end();
			}

			Interfaces::~Interfaces()
			{
				for(auto &interface : m_interfaces)
					delete interface;
			}
		}
	}
}
