#include <SystemX/Driver.hpp>

#include <SystemX/Logger.hpp>

#include <dlfcn.h>

namespace SystemX
{
	Driver::Driver(std::string name, std::string version)
		: m_name(name), m_version(version)
	{
	}

	void Driver::Initialize()
	{
	}

	void Driver::Initialize(Equipment::Equipment */*equipment*/)
	{
	}

	void Driver::Uninitialize(Equipment::Equipment */*equipment*/)
	{
	}

	void Driver::Uninitialize()
	{
	}

	void Driver::Add(Equipment::Equipment *equipment)
	{
		Initialize(equipment);
		m_equipments.insert(m_equipments.begin(), equipment);
	}

	void Driver::Remove(Equipment::Equipment *equipment)
	{
		for(size_t i = 0; i < m_equipments.size(); i++)
			if(m_equipments[i] == equipment)
			{
				Uninitialize(equipment);
				m_equipments.erase(m_equipments.begin() + i);
				break;
			}
	}

	std::string Driver::Name()
	{
		return m_name;
	}

	std::string Driver::Version()
	{
		return m_version;
	}

	Driver::~Driver()
	{
	}
}
