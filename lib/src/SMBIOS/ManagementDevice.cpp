#include <SystemX/SMBIOS/ManagementDevice.hpp>

namespace SystemX
{
	namespace SMBIOS
	{
		ManagementDevice::ManagementDevice(std::string description, Type type, uint32_t address, AddressType addressType)
			: m_description(description), m_type(type), m_address(address), m_addressType(addressType)
		{
		}

		std::string ManagementDevice::Description()
		{
			return m_description;
		}

		ManagementDevice::Type ManagementDevice::GetType()
		{
			return m_type;
		}

		uint32_t ManagementDevice::Address()
		{
			return m_address;
		}

		ManagementDevice::AddressType ManagementDevice::GetAddressType()
		{
			return m_addressType;
		}
	}
}
