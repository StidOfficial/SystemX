#include <SystemX/SMBIOS/CoolingDevice.hpp>

namespace SystemX
{
	namespace SMBIOS
	{
		CoolingDevice::CoolingDevice(uint16_t temperatureProbeHandle, uint8_t deviceTypeAndStatus, uint8_t coolingUnitGroup, uint32_t OEMDefined, uint16_t nominalSpeed,	
						std::string description)
			: m_temperatureProbeHandle(temperatureProbeHandle), m_deviceTypeAndStatus(deviceTypeAndStatus), m_coolingUnitGroup(coolingUnitGroup),
				m_OEMDefined(OEMDefined), m_nominalSpeed(nominalSpeed), m_description(description)
		{
		}

		uint16_t CoolingDevice::TemperatureProbeHandle()
		{
			return m_temperatureProbeHandle;
		}

		CoolingDevice::Type CoolingDevice::DeviceType()
		{
			return static_cast<Type>(m_deviceTypeAndStatus &= ~0 ^ (1 << 4));
		}

		CoolingDevice::Status CoolingDevice::DeviceStatus()
		{
			return static_cast<Status>((((1 << 5) - 1) & (m_deviceTypeAndStatus >> (7 - 1))));
		}

		uint8_t CoolingDevice::CoolingUnitGroup()
		{
			return m_coolingUnitGroup;
		}

		uint32_t CoolingDevice::OEMDefined()
		{
			return m_OEMDefined;
		}

		uint16_t CoolingDevice::NominalSpeed()
		{
			return m_nominalSpeed;
		}

		std::string CoolingDevice::Description()
		{
			return m_description;
		}
	}
}
