#include <SystemX/SMBIOS/SMBIOS.hpp>

#include <SystemX/Logger.hpp>
#include <SystemX/Utils.hpp>

#include <stdexcept>

namespace SystemX
{
	namespace SMBIOS
	{
		Type SMBIOS::m_type = Type::None;
		EntryPoint_2_1 *SMBIOS::m_entryPoint2_1 = nullptr;
		EntryPoint_3 *SMBIOS::m_entryPoint3 = nullptr;
		BIOSInformation *SMBIOS::m_biosInformation = nullptr;
		SystemInformation *SMBIOS::m_systemInformation = nullptr;
		BoardInformation *SMBIOS::m_boardInformation = nullptr;
		SystemEnclosureChassisInformation *SMBIOS::m_systemEnclosureChassisInformation = nullptr;
		std::vector<PortConnectorInformation*> SMBIOS::m_portsConnectorInformation;
		std::vector<SystemSlot*> SMBIOS::m_systemSlots;
		std::vector<std::string> SMBIOS::m_OEMStrings;
		std::vector<std::string> SMBIOS::m_systemConfigurationOptions;
		HardwareSecurity *SMBIOS::m_hardwareSecurity = nullptr;
		BIOSLanguageInformation *SMBIOS::m_BIOSLanguageInformation = nullptr;
		std::vector<ManagementDeviceThresholdData*> SMBIOS::m_managementsDeviceThreshold;
		std::vector<OnBoardDeviceInformation*> SMBIOS::m_onBoardDevicesInformation;
		std::vector<CoolingDevice*> SMBIOS::m_coolingDevices;
		SystemBootInformation *SMBIOS::m_systemBootInformation = nullptr;
		ManagementDevice *SMBIOS::m_managementDevice = nullptr;

		void* SMBIOS::m_structureTableAddr = nullptr;
		int SMBIOS::m_offset = 0;
		std::vector<std::string> SMBIOS::m_textTable;

		void SMBIOS::Initialize()
		{
			SystemX::Logger::Debug("Search SMBIOS Entry Point at 0x%x to 0x%x (%d)", SMBIOS_ENTRY_POINT_START_ADDR, SMBIOS_ENTRY_POINT_END_ADDR, SMBIOS_ENTRY_POINT_LENGTH);

			void *entryPointAddr = Utils::MemChunk(SMBIOS_ENTRY_POINT_START_ADDR, SMBIOS_ENTRY_POINT_LENGTH);
			if(!entryPointAddr)
				throw std::runtime_error("Failed to read SMBIOS Entry Point");

			void *addr = nullptr;
			for(int i = 0; i < SMBIOS_ENTRY_POINT_LENGTH; i+= 16)
				if(memcmp((((uint8_t*)entryPointAddr) + i), SMBIOS2_1_ANCHOR, strlen(SMBIOS2_1_ANCHOR)) == 0)
				{
					m_type = Type::x86;

					addr = (((uint8_t*)entryPointAddr) + i);
					SystemX::Logger::Debug("SMBIOS 2 found !");

					m_entryPoint2_1 = static_cast<EntryPoint_2_1*>(addr);

					/*SystemX::Logger::Debug("Anchor String: %.4s", m_entryPoint2_1->anchor);
					SystemX::Logger::Debug("Entry Point Structure Checksum : %d", m_entryPoint2_1->checksum);
					SystemX::Logger::Debug("Entry Point Length : %d", m_entryPoint2_1->length);*/
					SystemX::Logger::Debug("SMBIOS Major Version : %d", m_entryPoint2_1->majorVersion);
					SystemX::Logger::Debug("SMBIOS Minor Version : %d", m_entryPoint2_1->minorVersion);
					/*SystemX::Logger::Debug("Maximum Structure Size : %d", m_entryPoint2_1->maximumStructureSize);
					SystemX::Logger::Debug("Entry Point Revision : %d", m_entryPoint2_1->revision);
					SystemX::Logger::Debug("Formatted Aera : %d %d %d %d %d", m_entryPoint2_1->formattedAera[0], m_entryPoint2_1->formattedAera[1], m_entryPoint2_1->formattedAera[2], m_entryPoint2_1->formattedAera[3], m_entryPoint2_1->formattedAera[4]);
					SystemX::Logger::Debug("Intermediate Anchor String : %.5s", m_entryPoint2_1->intermediateAnchor);
					SystemX::Logger::Debug("Intermediat Checksum : %d", m_entryPoint2_1->intermediateChecksum);*/
					SystemX::Logger::Debug("Structure Table Length : %d", m_entryPoint2_1->structureTableLength);
					SystemX::Logger::Debug("Structure Table Address : 0x%x", m_entryPoint2_1->structureTableAddress);
					/*SystemX::Logger::Debug("Number Of SMBIOS Structure : %d", m_entryPoint2_1->numberOfSMBIOSStructure);
					SystemX::Logger::Debug("SMBIOS BCD Revision : %d", m_entryPoint2_1->BCDRevision);*/

					m_structureTableAddr = Utils::MemChunk(m_entryPoint2_1->structureTableAddress, m_entryPoint2_1->structureTableLength);

					StructureHeader *structureHeader;
					for(int i = 0; i < m_entryPoint2_1->numberOfSMBIOSStructure; i++)
					{
						structureHeader = static_cast<StructureHeader*>(m_structureTableAddr);

						/*SystemX::Logger::Debug("Type : %d", structureHeader->Type);
						SystemX::Logger::Debug("Length : %d", structureHeader->Length);
						SystemX::Logger::Debug("Handle : %x", structureHeader->Handle);*/

						int textTableLength = 0;
						m_textTable = TextTable(structureHeader->Length, &textTableLength);

						m_offset = sizeof(StructureHeader);
						switch(static_cast<StructureType>(structureHeader->Type))
						{
							case StructureType::BIOSInformation:
							{
								std::string vendor = GetString();
								std::string version = GetString();
								uint16_t startingAddressSegment = GetWord();
								std::string releaseDate = GetString();
								uint8_t ROMSize = GetByte();
								uint64_t characteristics = GetQWord();

								uint8_t characteristicsExtensionByte1, characteristicsExtensionByte2;
								if(MajorVersion() > 2 || (MajorVersion() >= 2 && MinorVersion() >= 1))
									characteristicsExtensionByte1 = GetByte();

								uint8_t systemBIOSMajorRelease, systemBIOSMinorRelease;
								uint8_t embeddedControllerFirmwareMajorRelease, embeddedControllerFirmwareMinorRelease;
								uint16_t extendedBIOSROMSize;
								if(MajorVersion() > 2 || (MajorVersion() >= 2 && MinorVersion() >= 4))
								{
									characteristicsExtensionByte2 = GetByte();

									systemBIOSMajorRelease = GetByte();
									systemBIOSMinorRelease = GetByte();

									embeddedControllerFirmwareMajorRelease = GetByte();
									embeddedControllerFirmwareMinorRelease = GetByte();
								}

								if(MajorVersion() > 3 || (MajorVersion() >= 3 && MinorVersion() >= 1))
									extendedBIOSROMSize = GetWord();

								m_biosInformation = new BIOSInformation(vendor, version,
													startingAddressSegment, releaseDate,
													ROMSize, characteristics, characteristicsExtensionByte1,
													characteristicsExtensionByte2, systemBIOSMajorRelease,
													systemBIOSMinorRelease, embeddedControllerFirmwareMajorRelease,
													embeddedControllerFirmwareMinorRelease, extendedBIOSROMSize);
								break;
							}
							case StructureType::SystemInformation: // 0xECF90
							{
								std::string manufacturer = GetString();
								std::string productName = GetString();
								std::string version = GetString();
								std::string serialNumber = GetString();

								SystemInformation::UniversalUniqueIdentifier UUID;
								SystemInformation::WakeUpType wakeUpType;
								if(MajorVersion() > 2 || (MajorVersion() >= 2 && MinorVersion() >= 1))
								{
									UUID = (*(((SystemInformation::UniversalUniqueIdentifier*)m_structureTableAddr) + m_offset));
									m_offset += sizeof(SystemInformation::UniversalUniqueIdentifier);

									wakeUpType = static_cast<SystemInformation::WakeUpType>(GetByte());
									m_offset += sizeof(uint8_t);
								}

								m_offset -= 1; // ???

								std::string SKUNumber;
								std::string family;
								if(MajorVersion() > 2 || (MajorVersion() >= 2 && MinorVersion() >= 4))
								{
									SKUNumber = GetString();
									family = GetString();
								}

								m_systemInformation = new SystemInformation(manufacturer, productName, version, serialNumber, UUID, wakeUpType,
													SKUNumber, family);
								break;
							}
							case StructureType::BoardInformation:
							{
								std::string manufacturer = GetString();
								std::string product = GetString();
								std::string version = GetString();
								std::string serialNumber = GetString();
								std::string assetTag = GetString();
								uint8_t featureFlags = GetByte();
								std::string locationInChassis = GetString();
								uint16_t chassisHandle = GetWord();
								BoardInformation::BoardType boardType = static_cast<BoardInformation::BoardType>(GetByte());

								std::vector<uint16_t> containedObjectHandles;

								uint8_t numberOfContainedObjectHandles = GetByte();
								for(int i = 0; i < numberOfContainedObjectHandles; i++)
									containedObjectHandles.push_back(GetWord());

								m_boardInformation = new BoardInformation(manufacturer, product, version, serialNumber, assetTag,
														featureFlags, locationInChassis, chassisHandle, boardType, containedObjectHandles);
								break;
							}
							case StructureType::SystemEnclosureChassisInformation:
							{
								std::string manufacturer = GetString();
								SystemEnclosureChassisInformation::Type type = static_cast<SystemEnclosureChassisInformation::Type>(GetByte());
								std::string version = GetString();
								std::string serialNumber = GetString();
								std::string assetTag = GetString();

								SystemEnclosureChassisInformation::State bootUpState, powerSupplyState, thermalState;
								SystemEnclosureChassisInformation::Status securityState;
								if(MajorVersion() > 2 || (MajorVersion() >= 2 && MinorVersion() >= 1))
								{
									bootUpState = static_cast<SystemEnclosureChassisInformation::State>(GetByte());
									powerSupplyState = static_cast<SystemEnclosureChassisInformation::State>(GetByte());
									thermalState = static_cast<SystemEnclosureChassisInformation::State>(GetByte());
									securityState = static_cast<SystemEnclosureChassisInformation::Status>(GetByte());
								}

								std::vector<uint8_t> containedElements;

								uint32_t OEMdefined;
								uint8_t height, numberOfPowerCords, containedElementCount, containedElementRecordLength;
								if(MajorVersion() > 2 || (MajorVersion() >= 2 && MinorVersion() >= 3))
								{
									OEMdefined = GetDWord();
									height = GetByte();
									numberOfPowerCords = GetByte();
									containedElementCount = GetByte();
									containedElementRecordLength = GetByte();

									int total = containedElementCount * containedElementRecordLength;
									for(int i = 0; i < total; i++)
										containedElements.push_back(GetByte());
								}

								std::string SKUNumber;
								if(MajorVersion() > 2 || (MajorVersion() >= 2 && MinorVersion() >= 3))
									SKUNumber = GetString();

								m_systemEnclosureChassisInformation = new SystemEnclosureChassisInformation(manufacturer, type, version, serialNumber,
																		assetTag, bootUpState, powerSupplyState,
																		thermalState, securityState, OEMdefined,
																		height, numberOfPowerCords, containedElements, SKUNumber);
								break;
							}
							case StructureType::ProcessorInformation:
							{
								SystemX::Logger::Warn("ProcessorInformation not fully supported");

								break;
							}
							case StructureType::CacheInformation:
							{
								SystemX::Logger::Warn("CacheInformation not fully supported");

								break;
							}
							case StructureType::PortConnectorInformation:
							{
								std::string internalReferenceDesignator = GetString();
								PortConnectorInformation::ConnectorType internalConnectorType = static_cast<PortConnectorInformation::ConnectorType>(GetByte());
								std::string externalReferenceDesignator = GetString();
								PortConnectorInformation::ConnectorType externalConnectorType = static_cast<PortConnectorInformation::ConnectorType>(GetByte());
								PortConnectorInformation::PortTypes portType = static_cast<PortConnectorInformation::PortTypes>(GetByte());

								m_portsConnectorInformation.push_back(new PortConnectorInformation(internalReferenceDesignator, internalConnectorType,
													externalReferenceDesignator, externalConnectorType, portType));
								break;
							}
							case StructureType::SystemSlots:
							{
								std::string slotDesignation = GetString();
								SystemSlot::Type slotType = static_cast<SystemSlot::Type>(GetByte());
								SystemSlot::SlotDataBusWidth slotDataBusWidth = static_cast<SystemSlot::SlotDataBusWidth>(GetByte());
								SystemSlot::CurrentUsage currentUsage = static_cast<SystemSlot::CurrentUsage>(GetByte());
								SystemSlot::Length slotLength = static_cast<SystemSlot::Length>(GetByte());
								uint16_t slotId = GetWord();
								uint8_t slotCharacteristics1 = GetByte();

								uint8_t slotCharacteristics2;
								if(MajorVersion() > 2 || (MajorVersion() >= 2 && MinorVersion() >= 1))
								{
									slotCharacteristics2 = GetByte();
								}

								uint16_t segmentGroupNumber;
								uint8_t busNumber;
								uint8_t deviceFunctionNumber;
								if(MajorVersion() > 2 || (MajorVersion() >= 2 && MinorVersion() >= 6))
								{
									segmentGroupNumber = GetWord();
									busNumber = GetByte();
									deviceFunctionNumber = GetByte();
								}

								std::vector<uint8_t> peerGroups;

								uint8_t dataBusWidth;
								if(MajorVersion() > 3 || (MajorVersion() >= 3 && MinorVersion() >= 2))
								{
									dataBusWidth = GetByte();
									uint8_t peerGroupingCount = GetByte();
									for(int i = 0; i < peerGroupingCount; i++)
										peerGroups.push_back(GetByte());
								}

								m_systemSlots.push_back(new SystemSlot(slotDesignation, slotType, slotDataBusWidth, currentUsage, slotLength,
													slotId, slotCharacteristics1, slotCharacteristics2, segmentGroupNumber,
													busNumber, deviceFunctionNumber, dataBusWidth, peerGroups));
								break;
							}
							case StructureType::OnBoardDevicesInformation:
							{
								uint8_t numDevices = (structureHeader->Length - 4) / 2;

								for(int i = 0; i < numDevices; i++)
								{
									uint8_t type = GetByte();
									std::string description = GetString();

									m_onBoardDevicesInformation.push_back(new OnBoardDeviceInformation(type, description));
								}
								break;
							}
							case StructureType::OEMStrings:
							{
								int8_t count = GetByte();
								for(int i = 0; i < count; i++)
									m_OEMStrings.push_back(m_textTable[i]);
								break;
							}
							case StructureType::SystemConfigurationOptions:
							{
								int8_t count = GetByte();
								for(int i = 0; i < count; i++)
									m_systemConfigurationOptions.push_back(m_textTable[i]);
								break;
							}
							case StructureType::BIOSLanguageInformation:
							{
								uint8_t installableLanguages = GetByte();
								uint8_t flags;
								if(MajorVersion() > 2 || (MajorVersion() >= 2 && MinorVersion() >= 1))
								{
									flags = GetByte();
								}
								std::vector<uint8_t> reserved;
								for(int i = 0; i < 15; i++)
									reserved.push_back(GetByte());
								uint8_t currentLanguage = GetByte();

								m_BIOSLanguageInformation = new BIOSLanguageInformation(installableLanguages, flags, currentLanguage, m_textTable);
								break;
							}
							case StructureType::PyshicalMemoryArray:
							{
								SystemX::Logger::Warn("PyshicalMemoryArray not fully supported");

								break;
							}
							case StructureType::MemoryDevice:
							{
								SystemX::Logger::Warn("MemoryDevice not fully supported");

								break;
							}
							case StructureType::MemoryArrayMappedAddress:
							{
								SystemX::Logger::Warn("MemoryDeviceMappedAddress not fully supported");

								break;
							}
							case StructureType::MemoryDeviceMappedAddress:
							{
								SystemX::Logger::Warn("MemoryDeviceMappedAddress not fully supported");

								break;
							}
							case StructureType::HardwareSecurity:
							{
								uint8_t hardwareSecuritySettings = GetByte();

								m_hardwareSecurity = new HardwareSecurity(hardwareSecuritySettings);
								break;
							}
							case StructureType::VoltageProbe:
							{
								SystemX::Logger::Warn("VoltageProbe not fully supported");

								break;
							}
							case StructureType::CoolingDevice:
							{
								uint16_t temperatureProbeHandle = GetWord();
								uint8_t deviceTypeAndStatus = GetByte();
								uint8_t coolingUnitGroup = GetByte();
								uint32_t OEMDefined = GetDWord();
								uint16_t nominalSpeed = GetWord();
								std::string description;
								if(MajorVersion() > 2 || (MajorVersion() >= 2 && MinorVersion() >= 7))
								{
									description = GetString();
								}

								m_coolingDevices.push_back(new CoolingDevice(temperatureProbeHandle, deviceTypeAndStatus, coolingUnitGroup,
														OEMDefined, nominalSpeed, description));
								break;
							}
							case StructureType::TemperatureProbe:
							{
								SystemX::Logger::Warn("TemperatureProbe not fully supported");

								break;
							}
							case StructureType::ElectricalCurrentProbe:
							{
								SystemX::Logger::Warn("ElectricalCurrentProbe not fully supported");

								break;
							}
							case StructureType::SystemBootInformation:
							{
								uint8_t reserved[6];
								for(int i = 0; i < 6; i++)
									reserved[i] = GetByte();

								uint8_t bootStatus[10];
								for(int i = 0; i < 10; i++)
									bootStatus[i] = GetByte();

								m_systemBootInformation = new SystemBootInformation(reserved, bootStatus);

								break;
							}
							case StructureType::ManagementDevice:
							{
								std::string description = GetString();
								uint8_t type = GetByte();
								uint32_t address = GetDWord();
								uint8_t addressType = GetByte();

								m_managementDevice = new ManagementDevice(description, (ManagementDevice::Type)type, address, (ManagementDevice::AddressType)addressType);

								break;
							}
							case StructureType::ManagementDeviceComponent:
							{
								SystemX::Logger::Warn("ManagementDeviceComponent not fully supported");

								break;
							}
							case StructureType::ManagementDeviceThresholdData:
							{
								uint16_t lowerThresholdNonCritical = GetWord();
								uint16_t upperThresholdNonCritical = GetWord();
								uint16_t lowerThresholdCritical = GetWord();
								uint16_t upperThresholdCritical = GetWord();
								uint16_t lowerThresholdNonRecoverable = GetWord();
								uint16_t upperThresholdNonRecoverable = GetWord();

								m_managementsDeviceThreshold.push_back(new ManagementDeviceThresholdData(lowerThresholdNonCritical, upperThresholdNonCritical,
																		lowerThresholdCritical, upperThresholdCritical,
																		lowerThresholdNonRecoverable, upperThresholdNonRecoverable));
								break;
							}
							case StructureType::SystemPowerSupply:
							{
								SystemX::Logger::Warn("SystemPowerSupply not fully supported");

								break;
							}
							case StructureType::OnboardDevicesExtendedInformation:
							{
								SystemX::Logger::Warn("OnboardDevicesExtendedInformation not fully supported");

								break;
							}
							case StructureType::EndOfTable:
							{
								break;
							}
							default:
							{
								SystemX::Logger::Debug("Type : %d", structureHeader->Type);
								SystemX::Logger::Debug("Length : %d", structureHeader->Length);
								SystemX::Logger::Debug("Handle : %x", structureHeader->Handle);
								SystemX::Logger::Error("Unknown type");
							}
						}

						m_structureTableAddr = (((uint8_t*)m_structureTableAddr) + structureHeader->Length);
						m_structureTableAddr = (((uint8_t*)m_structureTableAddr) + textTableLength);
					}

					break;
				}
				else if(memcmp((((uint8_t*)entryPointAddr) + i), SMBIOS3_ANCHOR, strlen(SMBIOS3_ANCHOR)) == 0)
				{
					m_type = Type::x86_64;

					addr = (((uint8_t*)entryPointAddr) + i);
					SystemX::Logger::Debug("SMBIOS 3 found  !");

					break;
				}
		}

		void SMBIOS::Destroy()
		{
			if(m_biosInformation)
				delete m_biosInformation;

			if(m_systemInformation)
				delete m_systemInformation;

			if(m_boardInformation)
				delete m_boardInformation;

			if(m_systemEnclosureChassisInformation)
				delete m_systemEnclosureChassisInformation;

			for(auto &portConnectorInformation : SMBIOS::m_portsConnectorInformation)
				delete portConnectorInformation;

			for(auto &systemSlot : m_systemSlots)
				delete systemSlot;

			if(m_hardwareSecurity)
				delete m_hardwareSecurity;

			if(m_BIOSLanguageInformation)
				delete m_BIOSLanguageInformation;

			for(auto &managementDeviceThreshold : m_managementsDeviceThreshold)
				delete managementDeviceThreshold;

			for(auto &onBoardDeviceInformation : m_onBoardDevicesInformation)
				delete onBoardDeviceInformation;

			for(auto &coolingDevice : m_coolingDevices)
				delete coolingDevice;

			delete m_systemBootInformation;
			delete m_managementDevice;
		}

		Type SMBIOS::GetType()
		{
			return m_type;
		}

		uint8_t SMBIOS::MajorVersion()
		{
			switch(m_type)
			{
				case Type::x86:
					return m_entryPoint2_1->majorVersion;
				case Type::x86_64:
					return m_entryPoint3->majorVersion;
				case Type::None:
				default:
					return -1;
			}
		}

		uint8_t SMBIOS::MinorVersion()
		{
			switch(m_type)
			{
				case Type::x86:
					return m_entryPoint2_1->minorVersion;
				case Type::x86_64:
					return m_entryPoint3->minorVersion;
				case Type::None:
				default:
					return -1;
			}
		}

		uint8_t SMBIOS::BCDRevision()
		{
			switch(m_type)
			{
				case Type::x86:
					return m_entryPoint2_1->BCDRevision;
				case Type::x86_64:
				case Type::None:
				default:
					return -1;
			}
		}

		BIOSInformation *SMBIOS::GetBIOSInformation()
		{
			return m_biosInformation;
		}

		SystemInformation *SMBIOS::GetSystemInformation()
		{
			return m_systemInformation;
		}

		BoardInformation *SMBIOS::GetBoardInformation()
		{
			return m_boardInformation;
		}

		SystemEnclosureChassisInformation *SMBIOS::GetSystemEnclosureChassisInformation()
		{
			return m_systemEnclosureChassisInformation;
		}

		std::vector<PortConnectorInformation*> SMBIOS::GetPortsConnectorInformation()
		{
			return m_portsConnectorInformation;
		}

		std::vector<SystemSlot*> SMBIOS::GetSystemSlots()
		{
			return m_systemSlots;
		}

		std::vector<std::string> SMBIOS::GetOEMStrings()
		{
			return m_OEMStrings;
		}

		std::vector<std::string> SMBIOS::GetSystemConfigurationOptions()
		{
			return m_systemConfigurationOptions;
		}

		HardwareSecurity *SMBIOS::GetHardwareSecurity()
		{
			return m_hardwareSecurity;
		}

		BIOSLanguageInformation *SMBIOS::GetBIOSLanguageInformation()
		{
			return m_BIOSLanguageInformation;
		}

		std::vector<ManagementDeviceThresholdData*> SMBIOS::GetManagementsDeviceThreshold()
		{
			return m_managementsDeviceThreshold;
		}

		std::vector<OnBoardDeviceInformation*> SMBIOS::GetOnBoardDevicesInformation()
		{
			return m_onBoardDevicesInformation;
		}

		std::vector<CoolingDevice*> SMBIOS::GetCoolingDevices()
		{
			return m_coolingDevices;
		}

		SystemBootInformation *SMBIOS::GetSystemBootInformation()
		{
			return m_systemBootInformation;
		}

		ManagementDevice *SMBIOS::GetManagementDevice()
		{
			return m_managementDevice;
		}

		uint8_t SMBIOS::GetByte()
		{
			uint8_t byte = (*(((uint8_t*)m_structureTableAddr) + m_offset));
			m_offset += sizeof(uint8_t);

			return byte;
		}

		uint16_t SMBIOS::GetWord()
		{
			uint16_t word = (*(((uint16_t*)m_structureTableAddr) + m_offset));
			m_offset += sizeof(uint16_t);

			return word;
		}

		uint32_t SMBIOS::GetDWord()
		{
			uint32_t dword = (*(((uint32_t*)m_structureTableAddr) + m_offset));
			m_offset += sizeof(uint32_t);

			return dword;
		}

		uint64_t SMBIOS::GetQWord()
		{
			uint64_t qword = (*(((uint64_t*)m_structureTableAddr) + m_offset));
			m_offset += sizeof(uint64_t);

			return qword;
		}

		std::string SMBIOS::GetString()
		{
			int index = GetByte();
			if(index <= 0)
				return std::string();

			return m_textTable[index - 1];
		}

		std::vector<std::string> SMBIOS::TextTable(size_t offset, int *length)
		{
			std::vector<std::string> textTable;

			int readLength = 0;
			for(;;)
			{
				int offsetStr = offset + readLength;
				char *str = (char*)m_structureTableAddr + offsetStr;
				size_t len = strlen(str);

				readLength += len + 1;

				if(len == 0)
					break;
				else
				{
					textTable.push_back(std::string(str));
				}
			}

			if(readLength == 1)
				readLength += 1;

			*length = *length + readLength;

			return textTable;
		}
	}
}
