#include <SystemX/SMBIOS/SystemSlot.hpp>

namespace SystemX
{
	namespace SMBIOS
	{
		SystemSlot::SystemSlot(std::string designation, Type type, SlotDataBusWidth slotDataBusWidth, CurrentUsage currentUsage, Length length, uint16_t id,
					uint8_t characteristics1, uint8_t characteristics2, uint16_t segmentGroupNumber, uint8_t busNumber, uint8_t deviceFunctionNumber,
					uint8_t dataBusWidth, std::vector<uint8_t> peerGroups)
			: m_designation(designation), m_type(type), m_slotDataBusWidth(slotDataBusWidth), m_currentUsage(currentUsage), m_length(length), m_id(id),
				m_characteristics1(characteristics1), m_characteristics2(characteristics2), m_segmentGroupNumber(segmentGroupNumber),
				m_busNumber(busNumber), m_deviceFunctionNumber(deviceFunctionNumber), m_dataBusWidth(dataBusWidth), m_peerGroups(peerGroups)
		{
		}

		std::string SystemSlot::Designation()
		{
			return m_designation;
		}

		SystemSlot::Type SystemSlot::GetType()
		{
			return m_type;
		}

		SystemSlot::SlotDataBusWidth SystemSlot::GetSlotDataBusWidth()
		{
			return m_slotDataBusWidth;
		}

		SystemSlot::CurrentUsage SystemSlot::GetCurrentUsage()
		{
			return m_currentUsage;
		}

		SystemSlot::Length SystemSlot::GetLength()
		{
			return m_length;
		}

		uint16_t SystemSlot::ID()
		{
			return m_id;
		}

		uint16_t SystemSlot::SegmentGroupNumber()
		{
			return m_segmentGroupNumber;
		}

		uint8_t SystemSlot::BusNumber()
		{
			return m_busNumber;
		}

		uint8_t SystemSlot::DeviceFunctionNumber()
		{
			return m_deviceFunctionNumber;
		}

		uint8_t SystemSlot::DataBusWidth()
		{
			return m_dataBusWidth;
		}

		std::vector<uint8_t> SystemSlot::PeerGroups()
		{
			return m_peerGroups;
		}
	}
}
