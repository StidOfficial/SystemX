#include <SystemX/SMBIOS/SystemInformation.hpp>

namespace SystemX
{
	namespace SMBIOS
	{
		SystemInformation::SystemInformation(std::string manufacturer, std::string productName,  std::string version,
						std::string serialNumber, UniversalUniqueIdentifier UUID,
						WakeUpType wakeUpType, std::string SKUNumber, std::string family)
			: m_manufacturer(manufacturer), m_productName(productName), m_version(version), m_serialNumber(serialNumber),
				m_UUID(UUID), m_wakeUpType(wakeUpType), m_SKUNumber(SKUNumber), m_family(family)
		{
		}

		std::string SystemInformation::Manufacturer()
		{
			return m_manufacturer;
		}

		std::string SystemInformation::ProductName()
		{
			return m_productName;
		}

		std::string SystemInformation::Version()
		{
			return m_version;
		}

		std::string SystemInformation::SerialNumber()
		{
			return m_serialNumber;
		}

		SystemInformation::UniversalUniqueIdentifier SystemInformation::UUID()
		{
			return m_UUID;
		}

		SystemInformation::WakeUpType SystemInformation::WakeUp()
		{
			return m_wakeUpType;
		}
	}
}
