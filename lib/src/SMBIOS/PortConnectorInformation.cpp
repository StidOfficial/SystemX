#include <SystemX/SMBIOS/PortConnectorInformation.hpp>

namespace SystemX
{
	namespace SMBIOS
	{
		PortConnectorInformation::PortConnectorInformation(std::string internalReferenceDesignator, ConnectorType internalConnectorType, std::string externalReferenceDesignator,
							ConnectorType externalConnectorType, PortTypes portType)
			: m_internalReferenceDesignator(internalReferenceDesignator), m_internalConnectorType(internalConnectorType),
				m_externalReferenceDesignator(externalReferenceDesignator), m_externalConnectorType(externalConnectorType), m_portType(portType)
		{
		}

		std::string PortConnectorInformation::InternalReferenceDesignator()
		{
			return m_internalReferenceDesignator;
		}

		PortConnectorInformation::ConnectorType PortConnectorInformation::InternalConnectorType()
		{
			return m_internalConnectorType;
		}

		std::string PortConnectorInformation::ExternalReferenceDesignator()
		{
			return m_externalReferenceDesignator;
		}

		PortConnectorInformation::ConnectorType PortConnectorInformation::ExternalConnectorType()
		{
			return m_externalConnectorType;
		}

		PortConnectorInformation::PortTypes PortConnectorInformation::GetPortType()
		{
			return m_portType;
		}
	}
}
