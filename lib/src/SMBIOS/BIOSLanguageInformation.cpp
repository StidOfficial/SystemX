#include <SystemX/SMBIOS/BIOSLanguageInformation.hpp>

namespace SystemX
{
	namespace SMBIOS
	{
		BIOSLanguageInformation::BIOSLanguageInformation(uint8_t installableLanguages, uint8_t flags, uint8_t currentLanguage, std::vector<std::string> languages)
			: m_installableLanguages(installableLanguages), m_flags(flags), m_currentLanguage(currentLanguage), m_languages(languages)
		{
		}

		uint8_t BIOSLanguageInformation::InstallableLanguages()
		{
			return m_installableLanguages;
		}

		std::string BIOSLanguageInformation::CurrentLanguage()
		{
			return m_languages[m_currentLanguage];
		}

		std::vector<std::string> BIOSLanguageInformation::Languages()
		{
			return m_languages;
		}
	}
}
