#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <string>
#include <cstdarg>

namespace SystemX
{
	class Logger
	{
	public:
		enum class LogLevel
		{
			TRACE,
			DEBUG,
			INFO,
			WARN,
			ERROR
		};

		static void Trace(std::string format, ...);
		static void Debug(std::string format, ...);
		static void Info(std::string format, ...);
		static void Warn(std::string format, ...);
		static void Error(std::string format, ...);
	private:
		static void Print(LogLevel level, std::string format, va_list args);
		static std::string GetLevel(LogLevel type);
	};
}

#endif
