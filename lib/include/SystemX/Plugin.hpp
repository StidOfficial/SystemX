#ifndef PLUGIN_HPP
#define PLUGIN_HPP

#include <string>

namespace SystemX
{
	class Plugin
	{
	public:
		Plugin(std::string name, std::string version) : m_name(name), m_version(version)
		{};
		virtual ~Plugin()
		{};
		std::string Name()
		{
			return m_name;
		}
		std::string Version()
		{
			return m_version;
		}
	private:
		std::string m_name;
		std::string m_version;
	};
}

#endif
