#ifndef NETWORK_UDPSERVER_HPP
#define NETWORK_UDPSERVER_HPP

#include "Socket.hpp"
#include "SocketServer.hpp"

namespace SystemX
{
	namespace Network
	{
		class UDPServer : private SocketServer<>
		{
		public:
			UDPServer(InetAddress address);
			UDPServer(ProtocolFamily family);

			virtual ssize_t RecvFrom(void *buffer, InetAddress &address);
			virtual ssize_t RecvFrom(void *buffer, std::size_t length, InetAddress &address);
			virtual ssize_t SendTo(const void *buffer, InetAddress address);
			virtual ssize_t SendTo(const void *buffer, std::size_t length, InetAddress address);
			virtual bool IsShutdown();
			virtual void Shutdown();
			virtual void Close();
		};
	}
}

#endif
