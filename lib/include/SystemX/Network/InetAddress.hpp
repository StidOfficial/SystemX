#ifndef NETWORK_INETADDRESS_HPP
#define NETWORK_INETADDRESS_HPP

#include "Network.hpp"

#include <cstdint>
#include <string>
#include <vector>

namespace SystemX
{
	namespace Network
	{
		enum class IPAddress
		{
			IPv4Any,
			IPv6Any
		};
	
		class InetAddress
		{
		public:
			InetAddress();
			InetAddress(struct sockaddr addr, socklen_t len);
			InetAddress(IPAddress address, int port);
			InetAddress(sa_family_t family);
			InetAddress(AddressFamily family);
			//InetAddress(sa_family_t family, char *address);
			InetAddress(AddressFamily family, std::string address, uint16_t port);

			AddressFamily GetAddressFamily();
			ProtocolFamily GetProtocolFamily();
			bool IsIPv4();
			bool IsIPv6();
			void SetPort(uint16_t port);
			uint16_t GetPort();
			void SetAddress(std::string address);
			void SetAddress(struct in_addr address);
			void SetAddress(struct in6_addr address);
			std::string GetAddress();
			std::string GetHostname();
			struct sockaddr *GetSocketAddress();
			socklen_t GetSocketAddressLength();

			static std::vector<InetAddress> GetHostByName(std::string hostname);
			static std::vector<InetAddress> GetAddressInfo(std::string hostname, AddressFamily family = AddressFamily::Unspecified);
		private:
			struct sockaddr m_sockaddr;
			socklen_t m_socklen;
		};
	}
}

#endif
