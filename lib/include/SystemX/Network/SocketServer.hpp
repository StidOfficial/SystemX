#ifndef NETWORK_SOCKETSERVER_HPP
#define NETWORK_SOCKETSERVER_HPP

#include "Network.hpp"
#include "InetAddress.hpp"

#include "../Logger.hpp"
#include "../Utils.hpp"

#include <cstddef>
#include <string>
#include <vector>
#include <mutex>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdexcept>
#include <cstring>
#include <thread>
#include <algorithm>

namespace SystemX
{
	namespace Network
	{
		class Socket;

		template<class T=Socket>
		class SocketServer
		{
		public:
			SocketServer(ProtocolFamily domain, SocketType type, ProtocolType protocol)
				: m_shutdown(false)
			{
			    m_socket = socket(static_cast<int>(domain), static_cast<int>(type), static_cast<int>(protocol));
			    if(m_socket < 0)
						throw std::runtime_error("socket() failed: " + Utils::LastError());
			}

			void Bind(InetAddress address)
			{
				int enable = 1;
				if(setsockopt(m_socket, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable)) < 0)
					throw std::runtime_error("setsockopt() failed: " + Utils::LastError());

				int result = bind(m_socket, address.GetSocketAddress(), address.GetSocketAddressLength());
				if(result < 0)
					throw std::runtime_error("bind() failed: " + Utils::LastError());
			}

			T* Accept()
			{
				struct sockaddr sockaddr;
				socklen_t socklen = sizeof(sockaddr);

				int socket = accept4(m_socket, &sockaddr, &socklen, 0);
				if(socket < 0 && IsShutdown())
					return nullptr;

				if(socket < 0)
					throw std::runtime_error("accept4() failed: " + Utils::LastError());

				return new T(socket, new InetAddress(sockaddr, socklen), reinterpret_cast<SocketServer<Socket>*>(this));
			}

			virtual void Open(T* socket)
			{
				m_mutex.lock();
				m_sockets.push_back(socket);
				m_mutex.unlock();

				socket->ReceiveLoop();
				socket->Close();

				m_mutex.lock();
				auto it = std::find(m_sockets.begin(), m_sockets.end(), socket);
				if(it != m_sockets.end())
					m_sockets.erase(it);
				m_mutex.unlock();

				delete socket;
			}

			void Listen()
			{
				while(!IsShutdown())
				{
					try
					{
						T* client = Accept();
						if(!client)
							continue;

						std::thread([client, this] {
							Open(client);
						}).detach();
					}
					catch(std::exception& e)
					{
						SystemX::Logger::Error("Listen : %s", e.what());
					}
				}
			}

			void Listen(int backlog)
			{
				int result = listen(m_socket, backlog);
				if(result < 0)
					throw std::runtime_error("listen() failed: " + Utils::LastError());

				Listen();
			}

			std::vector<T*> GetSockets()
			{
				return m_sockets;
			}

			ssize_t RecvFrom(void* buffer, InetAddress &address)
			{
				return RecvFrom(buffer, sizeof(buffer), address);
			}

			ssize_t RecvFrom(void* buffer, std::size_t length, InetAddress &address)
			{
				struct sockaddr sockaddr;
				socklen_t socklen = sizeof(sockaddr);

				int result = recvfrom(m_socket, buffer, length, 0, &sockaddr, &socklen);
				if(result < 0 && IsShutdown())
					return -1;

				if(result < 0)
					throw std::runtime_error("recvfrom() failed: " + Utils::LastError());

				address = InetAddress(sockaddr, socklen);

				return result;
			}

			ssize_t SendTo(const void* buffer, InetAddress address)
			{
				return SendTo(buffer, sizeof(buffer), address);
			}

			ssize_t SendTo(const void* buffer, std::size_t length, InetAddress address)
			{
				int result = sendto(m_socket, buffer, length, 0, address.GetSocketAddress(), address.GetSocketAddressLength());
				if(IsShutdown())
					return -1;

				if(result < 0)
					throw std::runtime_error("sendto() failed: " + Utils::LastError());

				return result;
			}

			void Broadcast(const void* buffer)
			{
				Broadcast(buffer, sizeof(buffer));
			}

			void Broadcast(const void* buffer, std::size_t length)
			{
				for(auto &socket : m_sockets)
					socket->Send(buffer, length);
			}

			bool IsShutdown()
			{
				return m_shutdown;
			}

			void Shutdown()
			{
				m_shutdown = true;
				shutdown(m_socket, SHUT_RDWR);
			}

			void Close()
			{
				Shutdown();
				close(m_socket);
			}

			virtual ~SocketServer()
			{
				Close();

				for(auto &socket : m_sockets)
					socket->Shutdown();
			}
		private:
			int m_socket;
			struct sockaddr_in m_socket_address;
			std::vector<T*> m_sockets;
			bool m_shutdown;
			std::mutex m_mutex;

			void Accept(T *socket);
		};
	}
}

#endif
