#ifndef NETWORK_NETWORK_HPP
#define NETWORK_NETWORK_HPP

#include <netdb.h>

namespace SystemX
{
	namespace Network
	{
		enum class AddressFamily
		{
			Unspecified = AF_UNSPEC,
			Unix = AF_UNIX,
			Local = AF_LOCAL,
			IPv4 = AF_INET,
			IPv6 = AF_INET6
		};

		enum class ProtocolFamily
		{
			Unspecified = PF_UNSPEC,
			Unix = PF_UNIX,
			Local = PF_LOCAL,
			IPv4 = PF_INET,
			IPv6 = PF_INET6
		};

		enum class SocketType
		{
			Stream = SOCK_STREAM,
			Datagram = SOCK_DGRAM
		};

		enum class ProtocolType
		{
			TCP = IPPROTO_TCP,
			UDP = IPPROTO_UDP
		};
	}
}

#endif
