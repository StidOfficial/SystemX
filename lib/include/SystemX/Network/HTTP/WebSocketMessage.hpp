#ifndef NETWORK_HTTP_WEBSOCKETMESSAGE_HPP
#define NETWORK_HTTP_WEBSOCKETMESSAGE_HPP

#include "Client.hpp"
#include "HTTP.hpp"

#include <vector>

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			class WebSocketMessage
			{
			public:
				enum OpCode {
					Continue = 0x0,
					Text = 0x1,
					Binary = 0x2,
					Close = 0x8,
					Ping = 0x9,
					Pong = 0xA
				};

				enum CloseCode {
					NormalClosure = 1000,
					GoingAway = 1001,
					ProtocolError = 1002,
					UnsupportedData = 1003,
					NoStatusRecvd = 1005,
					Abnormal = 1006,
					InvalidFramePayloadData = 1007,
					PolicyViolation = 1008,
					TooLarge = 1009
				};
			public:
				WebSocketMessage(Client *client);

				OpCode GetOpCode();
				std::vector<unsigned char> GetPayload();
			private:
				OpCode m_opCode;
				bool m_isMasked;
				unsigned char m_maskingKey[HTTP_WEBSOCKET_MASKING_KEY_LENGTH];
				std::vector<unsigned char> m_payload;
			};
		}
	}
}

#endif
