#ifndef NETWORK_HTTP_REQUEST_HPP
#define NETWORK_HTTP_REQUEST_HPP

#include <SystemX/Network/HTTP/Headers.hpp>
#include <SystemX/Network/HTTP/Cookies.hpp>
#include <SystemX/Network/URI.hpp>

#include <string>

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			class Client;

			enum class Method
			{
				GET,
				HEAD,
				POST,
				PUT,
				DELETE,
				TRACE,
				OPTIONS,
				CONNECT,
				PATCH
			};

			class Request
			{
			public:
				Request(Client *client);
				Request(Method method, Network::URI uri, HTTP::Headers headers);
				Request(Method method, Network::URI uri, HTTP::Headers headers, Cookies *cookies);
				~Request();

				void SetProtocolVersion(std::string protocolVersion);
				std::string GetProtocolVersion();
				void SetMethod(Method method);
				Method GetMethod();
				Network::URI Uri();
				HTTP::Headers* Headers();
				std::string ToString();

				static Method StringTo(std::string method);
				static std::string MethodTo(Method method);
			private:
				bool m_parsed;
				Method m_method;
				Network::URI m_uri;
				std::string m_protocolVersion;
				HTTP::Headers *m_headers;
			};
		}
	}
}

#endif
