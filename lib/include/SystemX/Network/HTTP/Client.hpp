#ifndef NETWORK_HTTP_CLIENT_HPP
#define NETWORK_HTTP_CLIENT_HPP

#include <SystemX/Network/TCPClient.hpp>
#include <SystemX/Network/InetAddress.hpp>
#include <SystemX/Network/URI.hpp>
#include <SystemX/Network/HTTP/Headers.hpp>
#include <SystemX/Network/HTTP/Cookies.hpp>

#include <vector>
#include <map>

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			class Request;
			class Response;

			class Client : public SystemX::Network::TCPClient
			{
			public:
				Client();
				Client(int socket, SystemX::Network::InetAddress *address, SystemX::Network::SocketServer<SystemX::Network::Socket> *server);
				virtual ~Client();

				virtual void ReceiveLoop();

				Response *Send(Request request, Cookies *cookies = nullptr, std::vector<char> data = std::vector<char>());
				void Send(Response *response);
				void Send(Response *response, std::vector<char> data);
				void Send(std::string text);
				void Send(std::vector<char> data);
				void Send(std::vector<char> data, std::size_t len);
				void Send(const void *buf, std::size_t len);
				Response *Get(URI uri, Headers headers = Headers(), Cookies *cookies = nullptr);
				Response *Post(URI uri, Headers headers = Headers(), Cookies *cookies = nullptr, std::vector<char> data = std::vector<char>());
			};
		}
	}
}

#endif
