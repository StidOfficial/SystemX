#ifndef NETWORK_HTTP_HTTP_HPP
#define NETWORK_HTTP_HTTP_HPP

#include <ctime>
#include <string>

#define HTTP_DATE_FORMAT			"%a, %d %b %Y %T %Z"

#define HTTP_WEBSOCKET_MAGIC			"258EAFA5-E914-47DA-95CA-C5AB0DC85B11"

#define HTTP_WEBSOCKET_MASKING_KEY_LENGTH	4

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			time_t StringToTime(std::string datetime);
			std::string TimeToString(time_t datetime);
		}
	}
}

#endif
