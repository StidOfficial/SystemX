#ifndef NETWORK_HTTP_CONTROLLER_HPP
#define NETWORK_HTTP_CONTROLLER_HPP

#include "Client.hpp"
#include "Request.hpp"
#include "Response.hpp"

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			class Controller
			{
			public:
				Controller();
				virtual ~Controller();

				virtual void OnMessage(Client *client, std::vector<char> buffer) = 0;
				virtual void Get(Client *client, Request *request, Response *response);
				virtual void Head(Client *client, Request *request, Response *response);
				virtual void Post(Client *client, Request *request, Response *response);
				virtual void Put(Client *client, Request *request, Response *response);
				virtual void Delete(Client *client, Request *request, Response *response);
				virtual void Connect(Client *client, Request *request, Response *response);
				virtual void Options(Client *client, Request *request, Response *response);
				virtual void Trace(Client *client, Request *request, Response *response);
				virtual void Patch(Client *client, Request *request, Response *response);
				virtual void OnRequest(Client *client, Request *request, Response *response);
				void OnRequestWebSocket(Client *client, Request *request, Response *response);
				void OnRequestFile(Client *client, Request *request, Response *response);
			};
		}
	}
}

#endif
