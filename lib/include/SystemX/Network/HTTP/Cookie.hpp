#ifndef NETWORK_HTTP_COOKIE_HPP
#define NETWORK_HTTP_COOKIE_HPP

#include <string>

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			class Cookie
			{
			public:
				Cookie(std::string name, std::string value, std::string domain);
				Cookie(std::string name, std::string value, std::string expires, std::string path, std::string domain, bool secure, bool httpOnly);
				Cookie(std::string name, std::string value, time_t expires, std::string path, std::string domain, bool secure, bool httpOnly);

				std::string Name();
				std::string Value();
				time_t Expires();
				std::string Path();
				std::string Domain();
				bool IsSecure();
				bool IsHTTPOnly();

				static Cookie* Parse(std::string str, std::string defaultDomain);
			private:
				std::string m_name;
				std::string m_value;
				time_t m_expires;
				std::string m_path;
				std::string m_domain;
				bool m_secure;
				bool m_httpOnly;
			};
		}
	}
}

#endif
