#ifndef NETWORK_HTTP_COOKIES_HPP
#define NETWORK_HTTP_COOKIES_HPP

#include <SystemX/Network/HTTP/Cookie.hpp>

#include <string>
#include <vector>

namespace SystemX
{
	namespace Network
	{
		namespace HTTP
		{
			class Cookies
			{
			public:
				Cookies();
				~Cookies();

				void Set(Cookie *cookie);
				void Remove(std::string domain, std::string name);
				Cookie *Get(std::string domain, std::string name, std::string path);
				std::vector<Cookie*> Get(std::string domain, std::string path);
				std::string ToString(std::string domain, std::string path);
				std::size_t Size();
				operator std::vector<Cookie*>()
				{
					return m_cookies;
				}
			private:
				std::vector<Cookie*> m_cookies;
			};
		}
	}
}

#endif
