#ifndef EQUIPMENT_DRIVERS_HPP
#define EQUIPMENT_DRIVERS_HPP

#include <SystemX/Driver.hpp>
#include <SystemX/Equipment/Equipment.hpp>

#include <vector>

namespace SystemX
{
	class Driver;

	namespace Equipment
	{
		class Equipment;

		class Drivers
		{
		public:
			Drivers(Equipment *equipment);

			bool Exist(Driver *driver);
			void Add(Driver *driver);
			void Remove(Driver *driver);

			std::vector<Driver*>::iterator begin();
			std::vector<Driver*>::iterator end();
		protected:
			Equipment *m_equipment;
			std::vector<Driver*> m_drivers;
		};
	}
}

#endif
