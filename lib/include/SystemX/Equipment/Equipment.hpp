#ifndef EQUIPMENT_EQUIPMENT_HPP
#define EQUIPMENT_EQUIPMENT_HPP

#include <SystemX/Equipment/Hardware/Hardwares.hpp>
#include <SystemX/Equipment/OperatingSystem/OperatingSystem.hpp>
#include <SystemX/Equipment/OperatingSystem/Software/Softwares.hpp>
#include <SystemX/Equipment/Network/Interfaces.hpp>
#include <SystemX/Equipment/Drivers.hpp>

#include <string>
#include <vector>
#include <memory>

namespace SystemX
{
	namespace Equipment
	{
		class Drivers;

		enum class Type
		{
			Computer = 0,
			Server = 1,
			Hub = 2,
			Router = 3,
			Switch = 4
		};

		class Equipment
		{
		public:
			Equipment(Hardware::Hardwares hardwares, std::vector<OperatingSystem::OperatingSystem*> operating_systems);
			virtual ~Equipment();

			virtual int Id() = 0;
			virtual std::string GetName() = 0;
			virtual void SetType(Type type) = 0;
			virtual Type GetType() = 0;
			virtual void SetDescription(std::string description) = 0;
			virtual std::string GetDescription() = 0;
			Hardware::Hardwares Hardwares();
			std::vector<OperatingSystem::OperatingSystem*> OperatingSystems();
			Network::Interfaces *Interfaces();
			SystemX::Equipment::Drivers *Drivers();
		private:
			Hardware::Hardwares m_hardwares;
			std::vector<OperatingSystem::OperatingSystem*> m_operating_systems;
			std::unique_ptr<Network::Interfaces> m_interfaces;
			std::unique_ptr<SystemX::Equipment::Drivers> m_drivers;
		};
	}
}

#endif
