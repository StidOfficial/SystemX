#ifndef EQUIPMENT_HARDWARE_HARDWARES_HPP
#define EQUIPMENT_HARDWARE_HARDWARES_HPP

#include "CPU.hpp"
#include <vector>

namespace SystemX
{
	namespace Equipment
	{
		namespace Hardware
		{
			class Hardwares
			{
			public:
				Hardwares(std::vector<CPU> cpus);

				int CPUInstalled();
				int CPUAvalaible();
			private:
				std::vector<CPU> m_cpus;
			};
		}
	}
}

#endif