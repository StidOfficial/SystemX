#ifndef EQUIPMENT_LOCALINFORMATION_HPP
#define EQUIPMENT_LOCALINFORMATION_HPP

#include "Hardware/Hardwares.hpp"
#include "OperatingSystem/OperatingSystems.hpp"
#include "OperatingSystem/LocalOperatingSystem.hpp"

#include <vector>

namespace SystemX
{
	namespace Equipment
	{
		class LocalEquipment
		{
		public:
			static void Initialize();
			static Hardware::Hardwares *Hardwares();
			static OperatingSystem::OperatingSystems *OperatingSystems();
			static OperatingSystem::LocalOperatingSystem *CurrentOperatingSystem();
			static void Destroy();
		private:
			static Hardware::Hardwares *m_hardwares;
			static OperatingSystem::OperatingSystems *m_operating_systems;
			static OperatingSystem::LocalOperatingSystem *m_localOperatingSystem;
		};
	}
}

#endif
