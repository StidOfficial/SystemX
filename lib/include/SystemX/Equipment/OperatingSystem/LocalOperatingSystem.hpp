#ifndef EQUIPMENT_OPERATINGSYSTEM_LOCALOPERATINGSYSTEM_HPP
#define EQUIPMENT_OPERATINGSYSTEM_LOCALOPERATINGSYSTEM_HPP

#include "OperatingSystem.hpp"

#include <string>

namespace SystemX
{
	namespace Equipment
	{
		namespace OperatingSystem
		{
			class LocalOperatingSystem : public OperatingSystem
			{
			public:
				LocalOperatingSystem();
				virtual ~LocalOperatingSystem();

				virtual void SetHostname(std::string hostname);
			};
		}
	}
}

#endif
