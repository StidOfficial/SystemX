#ifndef EQUIPMENT_OPERATINGSYSTEM_OPERATINGSYSTEM_HPP
#define EQUIPMENT_OPERATINGSYSTEM_OPERATINGSYSTEM_HPP

#include "Software/Softwares.hpp"

#include <string>

namespace SystemX
{
	namespace Equipment
	{
		namespace OperatingSystem
		{
			class OperatingSystem
			{
			public:
				OperatingSystem();
				virtual ~OperatingSystem();

				virtual void SetHostname(std::string hostname);
				virtual std::string GetHostname();
				Software::Softwares *Softwares();
			private:
				std::string m_hostname;
				Software::Softwares *m_softwares;
			};
		}
	}
}

#endif
