#ifndef EQUIPMENT_NETWORK_INTERFACE_HPP
#define EQUIPMENT_NETWORK_INTERFACE_HPP

#include <string>

namespace SystemX
{
	namespace Equipment
	{
		namespace Network
		{
			class Interface
			{
      public:
        Interface(std::string name);
				void SetState(bool state);
				bool GetState();
				void SetName(std::string name);
				std::string GetName();
				void SetMACAddress(std::string macAddress);
				std::string GetMACAddress();
			private:
				bool m_state;
				std::string m_name;
				std::string m_macAddress;
			};
		}
	}
}

#endif
