#ifndef SMBIOS_ONBOARDDEVICEINFORMATION_HPP
#define SMBIOS_ONBOARDDEVICEINFORMATION_HPP

#include <cstdint>
#include <string>

namespace SystemX
{
	namespace SMBIOS
	{
		class OnBoardDeviceInformation
		{
		public:
			enum class Type
			{
				Other = 0x1,
				Unknown = 0x2,
				Video = 0x3,
				SCSIController = 0x4,
				Ethernet = 0x5,
				TokenRing = 0x6,
				Sound = 0x7,
				PATAController = 0x8,
				SATAController = 0x9,
				SASController = 0xA
			};
			OnBoardDeviceInformation(uint8_t type, std::string description);
			bool Status();
			Type GetType();
			std::string Description();
		private:
			uint8_t m_type;
			std::string m_description;
		};
	}
}

#endif
