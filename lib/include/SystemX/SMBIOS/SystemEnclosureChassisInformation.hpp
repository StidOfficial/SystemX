#ifndef SMBIOS_SYSTEMENCLOSURECHASSISINFORMATION_HPP
#define SMBIOS_SYSTEMENCLOSURECHASSISINFORMATION_HPP

#include <cstdint>
#include <string>
#include <vector>

namespace SystemX
{
	namespace SMBIOS
	{
		class SystemEnclosureChassisInformation
		{
		public:
			enum class Type
			{
				Other = 0x1,
				Unknown = 0x2,
				Desktop = 0x3,
				LowProfileDesktop = 0x4,
				PizzaBox = 0x5,
				MiniTower = 0x6,
				Tower = 0x7,
				Portable = 0x8,
				Laptop = 0x9,
				Notebook = 0xA,
				HandHeld = 0xB,
				DockingStation = 0xC,
				AllinOne = 0xD,
				SubNotebook = 0xE,
				SpaceSaving = 0xF,
				LunchBox = 0x10,
				MainServerChassis = 0x11,
				ExpansionChassis = 0x12,
				SubChassis = 0x13,
				BusExpansionChassis = 0x14,
				PeripheralChassis = 0x15,
				RAIDChassis = 0x16,
				RackMountChassis = 0x17,
				SealedCasePC = 0x18,
				MultiSystemChassis = 0x19,
				CompactPCI = 0x1A,
				AdvancedTCA = 0x1C,
				Blade = 0x1C,
				BladeEnclosure = 0x1D,
				Tablet = 0x1E,
				Convertible = 0x1F,
				Detachable = 0x20,
				IoTGateway = 0x21,
				EmbeddedPC = 0x22,
				MiniPC = 0x23,
				StickPC = 0x24
			};

			enum class State
			{
				Other = 0x1,
				Unknown = 0x2,
				Safe = 0x3,
				Warning = 0x4,
				Critical = 0x5,
				NonRecoverable = 0x6
			};

			enum class Status
			{
				Other = 0x1,
				Unknown = 0x2,
				None = 0x3,
				ExternalInterfaceLockedOut = 0x4,
				ExternalInterfaceEnabled = 0x5
			};

			SystemEnclosureChassisInformation(std::string manufacturer, Type type, std::string version, std::string serialNumber, std::string assetTag,
								State bootUpState, State powerSupplyState, State thermalState, Status securityState, uint32_t OEMdefined,
								uint8_t height, uint8_t numberOfPowerCords, std::vector<uint8_t> containedElements, std::string SKUNumber);
			std::string Manufacturer();
			bool IsLocked();
			Type GetType();
			std::string Version();
			std::string SerialNumber();
			std::string AssetTag();
			State BootUp();
			State PowerSupply();
			State Thermal();
			Status SecurityState();
			uint32_t OEMDefined();
			uint8_t Height();
			uint8_t NumberOfPowerCords();
			std::vector<uint8_t> ContainedElements();
			std::string SKUNumber();
		private:
			std::string m_manufacturer;
			Type m_type;
			std::string m_version, m_serialNumber, m_assetTag;
			State m_bootUpState, m_powerSupplyState, m_thermalState;
			Status m_securityState;
			uint32_t m_OEMdefined;
			uint8_t m_height, m_numberOfPowerCords;
			std::vector<uint8_t> m_containedElements;
			std::string m_SKUNumber;
		};
	}
}

#endif
