#ifndef SMBIOS_HARDWARESECURITY_HPP
#define SMBIOS_HARDWARESECURITY_HPP

#include <cstdint>

namespace SystemX
{
	namespace SMBIOS
	{
		class HardwareSecurity
		{
		public:
			HardwareSecurity(uint8_t hardwareSecuritySettings);
		private:
			uint8_t m_hardwareSecuritySettings;
		};
	}
}

#endif
