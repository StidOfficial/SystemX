#ifndef SMBIOS_MANAGEMENTDEVICE_HPP
#define SMBIOS_MANAGEMENTDEVICE_HPP

#include <cstdint>
#include <string>

namespace SystemX
{
	namespace SMBIOS
	{
		class ManagementDevice
		{
		public:
			enum class Type
			{
				Other = 0x01,
				Unknown = 0x02,
				NationalSemiconductorLM75 = 0x03,
				NationalSemiconductorLM78 = 0x04,
				NationalSemiconductorLM79 = 0x05,
				NationalSemiconductorLM80 = 0x06,
				NationalSemiconductorLM81 = 0x07,
				AnalogDevicesADM9240 = 0x08,
				DallasSemiconductorDS1780 = 0x09,
				Maxim1617 = 0x0A,
				GenesysGL518SM = 0x0B,
				WinbondW83781D = 0x0C,
				HoltekHT82H791 = 0x0D
			};

			enum class AddressType
			{
				Other = 0x01,
				Unknown = 0x02,
				IOPort = 0x03,
				Memory = 0x04,
				SMBus = 0x05
			};

			ManagementDevice(std::string description, Type type, uint32_t address, AddressType addressType);
			std::string Description();
			Type GetType();
			uint32_t Address();
			AddressType GetAddressType();
		private:
			std::string m_description;
			Type m_type;
			uint32_t m_address;
			AddressType m_addressType;
		};
	}
}

#endif
