#ifndef SMBIOS_SMBIOS_HPP
#define SMBIOS_SMBIOS_HPP

#include <SystemX/SMBIOS/BIOSInformation.hpp>
#include <SystemX/SMBIOS/SystemInformation.hpp>
#include <SystemX/SMBIOS/BoardInformation.hpp>
#include <SystemX/SMBIOS/SystemEnclosureChassisInformation.hpp>
#include <SystemX/SMBIOS/PortConnectorInformation.hpp>
#include <SystemX/SMBIOS/SystemSlot.hpp>
#include <SystemX/SMBIOS/HardwareSecurity.hpp>
#include <SystemX/SMBIOS/BIOSLanguageInformation.hpp>
#include <SystemX/SMBIOS/ManagementDeviceThresholdData.hpp>
#include <SystemX/SMBIOS/OnBoardDeviceInformation.hpp>
#include <SystemX/SMBIOS/CoolingDevice.hpp>
#include <SystemX/SMBIOS/SystemBootInformation.hpp>
#include <SystemX/SMBIOS/ManagementDevice.hpp>

#include <vector>
#include <cstdint>
#include <cstring>
#include <string>

#define SMBIOS_ENTRY_POINT_START_ADDR	0xf0000
#define SMBIOS_ENTRY_POINT_END_ADDR	0xfffff
#define SMBIOS_ENTRY_POINT_LENGTH	(SMBIOS_ENTRY_POINT_END_ADDR - SMBIOS_ENTRY_POINT_START_ADDR)

#define SMBIOS2_1_ANCHOR		"_SM_"
#define SMBIOS3_ANCHOR			"_SM3_"

namespace SystemX
{
	namespace SMBIOS
	{
		enum Type
		{
			None,
			x86,
			x86_64
		};

		struct EntryPoint_2_1
		{
			uint8_t anchor[4];
			uint8_t checksum;
			uint8_t length;
			uint8_t majorVersion;
			uint8_t minorVersion;
			uint16_t maximumStructureSize;
			uint8_t revision;
			uint8_t formattedAera[5];
			uint8_t intermediateAnchor[5];
			uint8_t intermediateChecksum;
			uint16_t structureTableLength;
			uint32_t structureTableAddress;
			uint16_t numberOfSMBIOSStructure;
			uint8_t BCDRevision;
		};

		struct EntryPoint_3
		{
			uint8_t anchor[5];
			uint8_t checksum;
			uint8_t length;
			uint8_t majorVersion;
			uint8_t minorVersion;
			uint8_t docrev;
			uint8_t revision;
			uint8_t reserved;
			uint32_t structureTableMaximumSize;
			uint64_t structureTableAddress;
		};

		enum class StructureType
		{
			BIOSInformation = 0,
			SystemInformation = 1,
			BoardInformation = 2,
			SystemEnclosureChassisInformation = 3,
			ProcessorInformation = 4,
			CacheInformation = 7,
			PortConnectorInformation = 8,
			SystemSlots = 9,
			OnBoardDevicesInformation = 10,
			OEMStrings = 11,
			SystemConfigurationOptions = 12,
			BIOSLanguageInformation = 13,
			PyshicalMemoryArray = 16,
			MemoryDevice = 17,
			MemoryArrayMappedAddress = 19,
			MemoryDeviceMappedAddress = 20,
			HardwareSecurity = 24,
			VoltageProbe = 26,
			CoolingDevice = 27,
			TemperatureProbe = 28,
			ElectricalCurrentProbe = 29,
			SystemBootInformation = 32,
			ManagementDevice = 34,
			ManagementDeviceComponent = 35,
			ManagementDeviceThresholdData = 36,
			SystemPowerSupply = 39,
			OnboardDevicesExtendedInformation = 41,
			EndOfTable = 127
		};

		struct StructureHeader
		{
			uint8_t Type;
			uint8_t Length;
			uint16_t Handle;
		};

		class SMBIOS
		{
		public:
			static void Initialize();
			static void Destroy();
			static Type GetType();
			static uint8_t MajorVersion();
			static uint8_t MinorVersion();
			static uint8_t BCDRevision();
			static BIOSInformation *GetBIOSInformation();
			static SystemInformation *GetSystemInformation();
			static BoardInformation *GetBoardInformation();
			static SystemEnclosureChassisInformation *GetSystemEnclosureChassisInformation();
			static std::vector<PortConnectorInformation*> GetPortsConnectorInformation();
			static std::vector<SystemSlot*> GetSystemSlots();
			static std::vector<std::string> GetOEMStrings();
			static std::vector<std::string> GetSystemConfigurationOptions();
			static HardwareSecurity *GetHardwareSecurity();
			static BIOSLanguageInformation *GetBIOSLanguageInformation();
			static std::vector<ManagementDeviceThresholdData*> GetManagementsDeviceThreshold();
			static std::vector<OnBoardDeviceInformation*> GetOnBoardDevicesInformation();
			static std::vector<CoolingDevice*> GetCoolingDevices();
			static SystemBootInformation *GetSystemBootInformation();
			static ManagementDevice *GetManagementDevice();
		private:
			static Type m_type;
			static EntryPoint_2_1 *m_entryPoint2_1;
			static EntryPoint_3 *m_entryPoint3;
			static BIOSInformation *m_biosInformation;
			static SystemInformation *m_systemInformation;
			static BoardInformation *m_boardInformation;
			static SystemEnclosureChassisInformation *m_systemEnclosureChassisInformation;
			static std::vector<PortConnectorInformation*> m_portsConnectorInformation;
			static std::vector<SystemSlot*> m_systemSlots;
			static std::vector<std::string> m_OEMStrings;
			static std::vector<std::string> m_systemConfigurationOptions;
			static HardwareSecurity *m_hardwareSecurity;
			static BIOSLanguageInformation *m_BIOSLanguageInformation;
			static std::vector<ManagementDeviceThresholdData*> m_managementsDeviceThreshold;
			static std::vector<OnBoardDeviceInformation*> m_onBoardDevicesInformation;
			static std::vector<CoolingDevice*> m_coolingDevices;
			static SystemBootInformation *m_systemBootInformation;
			static ManagementDevice *m_managementDevice;

			static void *m_structureTableAddr;
			static int m_offset;
			static std::vector<std::string> m_textTable;

			static uint8_t GetByte();
			static uint16_t GetWord();
			static uint32_t GetDWord();
			static uint64_t GetQWord();
			static std::string GetString();
			static std::vector<std::string> TextTable(size_t offset, int *length);
		};
	}
}

#endif
