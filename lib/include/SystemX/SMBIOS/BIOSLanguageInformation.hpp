#ifndef SMBIOS_BIOSLANGUAGEINFORMATION_HPP
#define SMBIOS_BIOSLANGUAGEINFORMATION_HPP

#include <cstdint>
#include <vector>
#include <string>

namespace SystemX
{
	namespace SMBIOS
	{
		class BIOSLanguageInformation
		{
		public:
			BIOSLanguageInformation(uint8_t installableLanguages, uint8_t flags, uint8_t currentLanguage, std::vector<std::string> languages);
			uint8_t InstallableLanguages();
			std::string CurrentLanguage();
			std::vector<std::string> Languages();
		private:
			uint8_t m_installableLanguages;
			uint8_t m_flags;
			uint8_t m_currentLanguage;
			std::vector<std::string> m_languages;
		};
	}
}

#endif
