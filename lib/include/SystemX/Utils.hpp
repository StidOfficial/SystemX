#ifndef UTILS_HPP
#define UTILS_HPP

#include <vector>
#include <string>
#include <regex>

namespace SystemX
{
	class Utils
	{
	public:
		static std::vector<std::string> Split(const std::string &text, const std::string &regex, const size_t limit = -1);
		static std::string Join(const std::vector<std::string> array, std::string delimiter);
		static std::string Trim(const std::string &text);
		static std::string Fill(const std::string &character, const int number);
		static std::string TableFill(const std::string &text, const int size);
		static std::string LastError();
		static std::string SHA1(const std::string str);
		static std::string Base64Encode(const std::string str);
		static void* MemChunk(const off_t offset, const size_t length);
	};
}

#endif
