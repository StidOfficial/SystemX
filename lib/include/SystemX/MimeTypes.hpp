#ifndef MIMETYPES_HPP
#define MIMETYPES_HPP

#include <map>
#include <string>
#include <experimental/filesystem>

namespace SystemX
{
	class MimeTypes
	{
	public:
		static std::string find(std::experimental::filesystem::path path);
	private:
		static const std::map<std::string, std::string> m_mimetypes;
	};
}

#endif
