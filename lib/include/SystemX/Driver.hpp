#ifndef DRIVER_HPP
#define DRIVER_HPP

#include <SystemX/Equipment/Equipment.hpp>

#include <string>
#include <vector>

namespace SystemX
{
	namespace Equipment
	{
		class Equipment;
	}

	class Driver
	{
	public:
		Driver(std::string name, std::string version);
		virtual ~Driver();

		virtual void Initialize();
		virtual void Initialize(Equipment::Equipment *equipment);
		virtual void Uninitialize(Equipment::Equipment *equipment);
		virtual void Uninitialize();

		void Add(Equipment::Equipment *equipment);
		void Remove(Equipment::Equipment *equipment);

		std::string Name();
		std::string Version();
	private:
		std::string m_name;
		std::string m_version;

		std::vector<Equipment::Equipment*> m_equipments;
	};
}

#endif
