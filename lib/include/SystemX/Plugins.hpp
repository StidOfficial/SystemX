#ifndef PLUGINS_HPP
#define PLUGINS_HPP

#include <SystemX/Plugin.hpp>

#include <string>
#include <vector>
#include <map>

#define SYSTEMX_PLUGIN(class)					\
	extern "C" {						\
		void plugin_register()				\
		{						\
			static class plugin;			\
			SystemX::Plugins::Register(&plugin);	\
		}						\
	}

namespace SystemX
{
	class Plugins
	{
	public:
		static void Initialize();
		static void Register(Plugin *plugin);
		static std::map<std::string, Plugin*> All();
		static void Destroy();
	private:
		static std::vector<void*> m_handlers;
		static std::map<std::string, Plugin*> m_plugins;
	};
}

#endif
