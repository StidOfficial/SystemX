class SystemX {
	connect(host) {
		this.socket = new WebSocket("ws://" + host);
	}

	login(username, password) {
		var loginPacket = new DataView(new ArrayBuffer(2 + 4 + username.length + 4 + password.length));
		loginPacket.setUint16(0, 1); // Id 1
		loginPacket.setUint32(2, username.length); // Username length
		for(var i = 0; i < username.length; i++)
		{
			console.log(i, username.charAt(i));
			loginPacket.setUint8(2 + 4 + i, username.charCodeAt(i));
		}
		loginPacket.setUint32(2 + 4 + username.length, password.length); // Password length
		for(var i = 0; i < password.length; i++)
			loginPacket.setUint8(2 + 4 + username.length + 4 + i, password.charCodeAt(i));

		this.socket.send(loginPacket);
	}
}
