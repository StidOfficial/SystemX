const routes = [
	{
		path: "/home",
		name: "home",
		components: {
			sidebar: sidebar,
			default: home
		}
	},
	{
		path: "/plugins",
		name: "plugins",
		components: {
			sidebar: sidebar,
			default: plugins
		}
	},
	{
		path: "/drivers",
		name: "drivers",
		components: {
			sidebar: sidebar,
			default: drivers
		}
	},
	{
		path: "/equipments",
		name: "equipments",
		component: equipments
	},
	{
		path: "/equipment/:uniqueId/overview",
		name: "equipmentOverview",
		components: {
			sidebar: sidebar,
			default: equipmentOverview
		}
	},
	{
		path: "/equipment/:uniqueId/informations",
		name: "equipmentInformations",
		components: {
			sidebar: sidebar,
			default: equipmentInformations
		}
	},
	{
		path: "/equipment/:uniqueId/smbios",
		name: "equipmentSMBIOS",
		components: {
			sidebar: sidebar,
			default: equipmentSMBIOS
		}
	},
	{
		path: "*",
		redirect: "/home"
	}
]

const router = new VueRouter({
	routes,
	mode: "hash",
	linkActiveClass: "active",
	linkExactActiveClass: "active"
})

const app = new Vue({
	router,
	el: "#app",
	components: {
		formSignin
	},
	data: {
		username: "admin"
	}
})

$("#loginModal").modal("show");
