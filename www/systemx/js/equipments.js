const equipments = {
	data: function() {
		return {
			equipments: []
		}
	},
	template: `
		<main role="main" class="col-md-12 ml-sm-auto col-lg-12 px-4">
			<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
				<h1 class="h2">Equipments</h1>

				<div class="btn-toolbar mb-2 mb-md-0">
					<div class="input-group mb-3">
						<input type="text" class="form-control" placeholder="Search..." aria-label="Recipient's username" aria-describedby="button-addon2">
						<div class="input-group-append">
							<button class="btn btn-outline-secondary" type="button" id="button-addon2">Search</button>
						</div>
					</div>
				</div>
			</div>
			<div class="card-deck card-deck-equipments">
				<div class="card mb-4" v-for="equipment in equipments" :class="{'border-danger': !equipment.status}">
					<div class="card-header">
						<router-link :to="{name: 'equipmentOverview', params: {uniqueId: equipment.uniqueId}}">
							<i class="fas fa-circle" :class="{'text-success': equipment.status, 'text-danger': !equipment.status}"></i> {{ equipment.name }}
						</router-link>
					</div>
					<div class="card-body">
						<p class="card-text">{{ equipment.description }}</p>
					</div>
				</div>
			</div>
		</main>
	`,
	mounted: function() {
		this.$http.get("/api/equipments", {params: {page: 1, search: null}}).then(response => {
			this.equipments = response.body
		})
	}
}
