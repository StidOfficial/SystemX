const plugins = {
	data: function() {
		return {
			plugins: []
		}
	},
	template: `
		<main role="main" class="col-md-10 ml-sm-auto col-lg-10 px-4">
			<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
				<h1 class="h2">Plugins</h1>
			</div>

			<table class="table">
				<thead>
					<tr>
						<th scope="col">Name</th>
						<th scope="col">Version</th>
					</tr>
				</thead>
				<tbody>
					<tr v-for="plugin in plugins">
						<td>{{ plugin.name }}</td>
						<td>{{ plugin.version }}</td>
					</tr>
				</tbody>
			</table>
		</main>
	`,
	mounted: function() {
		this.$http.get("/api/plugins").then(response => {
			this.plugins = response.body
		})
	}
}
