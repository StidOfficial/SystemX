#include <SystemX/Daemon/Manager/Manager.hpp>

#include <SystemX/Daemon/Manager/Equipments.hpp>
#include "CommandLine/Equipments.hpp"
#include "CommandLine/Plugins.hpp"
#include "CommandLine/Drivers.hpp"
#include "Network/HTTP/WebAdminController.hpp"

#include <SystemX/Logger.hpp>
#include <SystemX/Daemon/Manager/CommandLine/Interface.hpp>

#include <thread>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			SystemX::Database::Database Manager::m_database(SystemX::Database::Driver::SQLite);
			SystemX::Network::TCPServer<Network::TelnetClient> *Manager::m_telnet_server = nullptr;
			Network::SystemXServer *Manager::m_systemx_server = nullptr;
			SystemX::Network::HTTP::Server *Manager::m_http_server = nullptr;

			void Manager::Initialize()
			{
				SystemX::Logger::Info("Initialize manager...");

				SystemX::Logger::Info("Initialize database...");
				m_database.Open("systemxd.sqlite3");

				Equipments::Initialize();
				CommandLine::Interface::Register(InitializeInterface);

				m_telnet_server = new SystemX::Network::TCPServer<Network::TelnetClient>();
				m_systemx_server = new Network::SystemXServer(SystemX::Network::InetAddress(SystemX::Network::IPAddress::IPv4Any, 6000));
				m_http_server = new SystemX::Network::HTTP::Server();

				m_http_server->AddController("localhost", new Network::HTTP::WebAdminController());
			}

			void Manager::InitializeInterface(CommandLine::Namespace *base)
			{
				base->RegisterNamespace(new CommandLine::Equipments());
				base->RegisterNamespace(new CommandLine::Plugins());
				base->RegisterNamespace(new CommandLine::Drivers());
			}

			void Manager::Run()
			{
				std::thread telnetServerThread(RunTelnetServer);
				std::thread systemxServerThread(RunSystemXServer);
				std::thread webServerThread(RunWebServer);

				telnetServerThread.join();
				systemxServerThread.join();
				webServerThread.join();
			}

			SystemX::Database::Database *Manager::Database()
			{
				return &m_database;
			}

			SystemX::Network::TCPServer<Network::TelnetClient> *Manager::TelnetServer()
			{
				return m_telnet_server;
			}

			void Manager::RunTelnetServer()
			{
				SystemX::Logger::Info("Telnet server...");

				try
				{
					m_telnet_server->Bind(SystemX::Network::InetAddress(SystemX::Network::IPAddress::IPv4Any, 23));
					m_telnet_server->Listen(10);
				}
				catch(std::exception &e)
				{
					SystemX::Logger::Error("Failed: %s", e.what());
					m_telnet_server->Close();
				}

				SystemX::Logger::Info("Telnet server shutdown !");
			}

			void Manager::RunSystemXServer()
			{
				SystemX::Logger::Info("SystemX Server...");

				m_systemx_server->Listen();

				SystemX::Logger::Info("SystemX Server shutdown !");
			}


			void Manager::RunWebServer()
			{
				SystemX::Logger::Info("Web Server...");

				try
				{
					m_http_server->Bind(SystemX::Network::InetAddress(SystemX::Network::IPAddress::IPv4Any, 80));
					m_http_server->Listen(10);
				}
				catch(std::exception &e)
				{
					SystemX::Logger::Error("Failed: %s", e.what());
					m_http_server->Close();
				}

				SystemX::Logger::Info("Web Server shutdown !");
			}

			void Manager::Shutdown()
			{
				m_telnet_server->Close();
				m_systemx_server->Close();
				m_http_server->Close();
			}

			void Manager::Destroy()
			{
				delete m_telnet_server;
				delete m_systemx_server;
				delete m_http_server;

				Equipments::Destroy();

				m_database.Close();
			}
		}
	}
}
