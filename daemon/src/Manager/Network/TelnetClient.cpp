#include <SystemX/Daemon/Manager/Network/TelnetClient.hpp>

#include <SystemX/Daemon/Manager/Manager.hpp>

#include <SystemX/Utils.hpp>
#include <SystemX/Logger.hpp>
#include <SystemX/Database/DatabaseStatement.hpp>
#include <SystemX/Equipment/LocalEquipment.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				TelnetClient::TelnetClient(int socket, SystemX::Network::InetAddress *address, SystemX::Network::SocketServer<SystemX::Network::Socket> *server)
					: SystemX::Network::TelnetClient(socket, address, server)
				{
					m_interface = new CommandLine::Interface();
				}

				void TelnetClient::ReceiveLoop()
				{
					ssize_t size;

					try
					{
						std::vector<char> buffer_comand(1024);
						size = Recv(&buffer_comand[0], buffer_comand.size());
						buffer_comand.resize(size);

						std::string login, password;

						SetOption(SystemX::Network::TelnetCommand::DO, SystemX::Network::TelnetOption::SuppressGoAhead);
						SendOption();

						SetOption(SystemX::Network::TelnetCommand::WILL, SystemX::Network::TelnetOption::SuppressGoAhead);
						SetOption(SystemX::Network::TelnetCommand::WILL, SystemX::Network::TelnetOption::Echo);
						SetOption(SystemX::Network::TelnetCommand::DONT, SystemX::Network::TelnetOption::TerminalType);
						SetOption(SystemX::Network::TelnetCommand::DONT, SystemX::Network::TelnetOption::NegotiateAboutWindowSize);
						SetOption(SystemX::Network::TelnetCommand::DONT, SystemX::Network::TelnetOption::TerminalSpeed);
						SetOption(SystemX::Network::TelnetCommand::DONT, SystemX::Network::TelnetOption::RemoteFlowControl);
						SetOption(SystemX::Network::TelnetCommand::DONT, SystemX::Network::TelnetOption::Linemode);
						SetOption(SystemX::Network::TelnetCommand::DONT, SystemX::Network::TelnetOption::NewEnvironmentOption);
						SetOption(SystemX::Network::TelnetCommand::WILL, SystemX::Network::TelnetOption::Status);
						SetOption(SystemX::Network::TelnetCommand::DONT, SystemX::Network::TelnetOption::XDisplayLocation);
						SendOption();

						ReadIAC();
						ReadIAC();

						Send("\r\n");
						Send("Login : ");
						login = ReadLine();

						Send("\r\n");
						Send("Password : ");
						password = ReadPassword();

						SystemX::Database::DatabaseStatement *statement = Manager::Manager::Database()->Prepare("SELECT id FROM users WHERE username = :username AND password = :password;");
						statement->Bind(":username", login);
						statement->Bind(":password", password);

						if(statement->First())
						{
							delete statement;

							while(IsConnected())
							{
								Send("\r\n");
								Send(login + "@" + SystemX::Equipment::LocalEquipment::CurrentOperatingSystem()->GetHostname() + CommandInterface()->Path() + "#");

								std::string commandLine = ReadCommand();
								processcommand:
								std::vector<std::string> commandArgs = SystemX::Utils::Split(commandLine, " ");

								std::string command = commandArgs[0];
								commandArgs.erase(commandArgs.begin());

								if(!command.empty())
								{
									if(CommandInterface()->CurrentNamespace()->ExistCommand(command))
										CommandInterface()->CurrentNamespace()->GetCommand(command)(this, commandArgs);
									else if(CommandInterface()->CurrentNamespace()->ExistNamespace(command))
										CommandInterface()->ChangeNamespace(CommandInterface()->CurrentNamespace()->GetNamespace(command));
									else
									{
										if(command.length() >= 2)
										{
											std::string commandFound = CommandInterface()->CurrentNamespace()->AutoComplete(command);
											if(!commandFound.empty() && commandFound != command)
											{
												commandLine = commandFound;
												goto processcommand;
											}
										}

										Send("\r\n" + command + ": command not found !");
									}
								}
							}
						}
						else
						{
							delete statement;

							Send("\r\n\r\nInvalid login or password !\r\n");
							Close();
						}
					}
					catch(std::exception &e)
					{
					}
				}

				std::string TelnetClient::ReadLine()
				{
					return ReadLine(false);
				}

				std::string TelnetClient::ReadLine(bool password)
				{
					std::string line;

					while(true)
					{
						std::vector<char> buffer(255);
						buffer.resize(Recv(&buffer[0], buffer.size()));

						if(buffer.size() == 1)
						{
							char character = buffer[0];

							if(character >= 32 && character <= 126)
							{
								line.push_back(character);
								if(!password)
									Send(&character, 1);
							}
							else if(character == 127 && line.length() > 0)
							{
								line.pop_back();
								if(!password)
									Send("\b \b");
							}
						}
						else if(buffer.size() == 2)
						{
							if(buffer[0] == '\r' && buffer[1] == '\0')
								break;
						}
						else
						line.append(buffer.begin(), buffer.end());
					}

					return line;
				}

				std::string TelnetClient::ReadPassword()
				{
					return ReadLine(true);
				}

				std::string TelnetClient::ReadCommand()
				{
					std::string line;

					while(true)
					{
						std::vector<char> buffer(255);
						buffer.resize(Recv(&buffer[0], buffer.size()));

						if(buffer.size() == 1)
						{
							char character = buffer[0];

							if(character == 0x03)
								return "";
							else if(character >= 32 && character <= 126)
							{
								line.push_back(character);
								Send(&character, 1);
							}
							else if(character == 127 && line.length() > 0)
							{
								line.pop_back();
								Send("\b \b");
							}
							else if(character == '\t')
							{
								std::string command = CommandInterface()->CurrentNamespace()->AutoComplete(line);
								if(!command.empty())
								{
									std::string rest = command.substr(line.length());
									line.append(rest);
									Send(rest);
								}
							}
						}
						else if(buffer.size() == 2)
						{
							if(buffer[0] == '\r' && buffer[1] == '\0')
								break;
						}
						else if(buffer.size() == 3 && buffer[0] == 0x1b && buffer[1] == 0x5b)
						{
							/*
							if(buffer[2] == 43)
							// Return history command
							*/
						}
						else
						{
							std::string command = std::string(buffer.begin(), buffer.end());
							line.append(command);
							Send(command);
						}
					}

					return line;
				}

				CommandLine::Interface *TelnetClient::CommandInterface()
				{
					return m_interface;
				}

				TelnetClient::~TelnetClient()
				{
					delete m_interface;
				}
			}
		}
	}
}
