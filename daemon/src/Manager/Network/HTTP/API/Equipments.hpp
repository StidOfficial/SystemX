#ifndef DAEMON_MANAGER_NETWORK_HTTP_API_EQUIPMENTS_HPP
#define DAEMON_MANAGER_NETWORK_HTTP_API_EQUIPMENTS_HPP

#include "SessionController.hpp"

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				namespace HTTP
				{
					namespace API
					{
						class Equipments : public SessionController
						{
						public:
							Equipments();
							~Equipments();

							void Get(SystemX::Network::HTTP::Client *client, SystemX::Network::HTTP::Request *request, SystemX::Network::HTTP::Response *response);
						};
					}
				}
			}
		}
	}
}

#endif
