#include "Drivers.hpp"

#include "Controller.hpp"

#include <json/json.h>
#include <SystemX/Drivers.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				namespace HTTP
				{
					namespace API
					{
						Drivers::Drivers()
							: SessionController()
						{
						}

						void Drivers::Get(SystemX::Network::HTTP::Client *client, SystemX::Network::HTTP::Request *request, SystemX::Network::HTTP::Response *response)
						{
							if(IsAuthenticated())
							{
								Json::Value root(Json::arrayValue);

								for(const auto& driver : SystemX::Drivers::All())
								{
									Json::Value obj_driver;
									obj_driver["name"] = driver.second->Name();
									obj_driver["version"] = driver.second->Version();

									root.append(obj_driver);
								}

								API::Controller::SendToClient(200, client, response, root);
							}
							else
								API::Controller::SendToClient(403, client, response);
						}

						Drivers::~Drivers()
						{
						}
					}
				}
			}
		}
	}
}
