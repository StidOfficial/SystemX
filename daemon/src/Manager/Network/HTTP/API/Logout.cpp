#include "Logout.hpp"

#include "Controller.hpp"

#include <json/json.h>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				namespace HTTP
				{
					namespace API
					{
						Logout::Logout()
							: SessionController()
						{
						}

						void Logout::Post(SystemX::Network::HTTP::Client *client, SystemX::Network::HTTP::Request *request, SystemX::Network::HTTP::Response *response)
						{
							if(IsAuthenticated())
							{
								Logout();

								Json::Value root = true;
								API::Controller::SendToClient(200, client, response, root);
							}
							else
								API::Controller::SendToClient(403, client, response);
						}

						Logout::~Logout()
						{
						}
					}
				}
			}
		}
	}
}
