#ifndef DAEMON_MANAGER_NETWORK_HTTP_API_AUTHENTICATION_HPP
#define DAEMON_MANAGER_NETWORK_HTTP_API_AUTHENTICATION_HPP

#include "SessionController.hpp"

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				namespace HTTP
				{
					namespace API
					{
						class Authentication : public SessionController
						{
						public:
							Authentication();
							~Authentication();

							void Post(SystemX::Network::HTTP::Client *client, SystemX::Network::HTTP::Request *request, SystemX::Network::HTTP::Response *response);
						};
					}
				}
			}
		}
	}
}

#endif
