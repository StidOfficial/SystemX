#ifndef DAEMON_MANAGER_NETWORK_HTTP_API_AUTHENTICATED_HPP
#define DAEMON_MANAGER_NETWORK_HTTP_API_AUTHENTICATED_HPP

#include "SessionController.hpp"

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				namespace HTTP
				{
					namespace API
					{
						class Authenticated : public SessionController
						{
						public:
							Authenticated();
							~Authenticated();

							void Get(SystemX::Network::HTTP::Client *client, SystemX::Network::HTTP::Request *request, SystemX::Network::HTTP::Response *response);
						};
					}
				}
			}
		}
	}
}

#endif
