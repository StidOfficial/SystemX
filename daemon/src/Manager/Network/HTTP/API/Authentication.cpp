#include "Authentication.hpp"

#include "Controller.hpp"

#include <json/json.h>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				namespace HTTP
				{
					namespace API
					{
						Authentication::Authentication()
							: SessionController()
						{
						}

						void Authentication::Post(SystemX::Network::HTTP::Client *client, SystemX::Network::HTTP::Request *request, SystemX::Network::HTTP::Response *response)
						{
							if(!IsAuthenticated())
							{
								Json::Value root;
								API::Controller::SendToClient(200, client, response, root);
							}
							else
							API::Controller::SendToClient(400, client, response);
						}

						Authentication::~Authentication()
						{
						}
					}
				}
			}
		}
	}
}
