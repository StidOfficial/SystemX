#include "SessionController.hpp"

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				namespace HTTP
				{
					namespace API
					{
						SessionController::SessionController() :
							SystemX::Network::HTTP::Controller()
						{
						}

						bool SessionController::IsAuthenticated()
						{
							return true;
						}

						void SessionController::Logout()
						{
						}

						void SessionController::OnMessage(SystemX::Network::HTTP::Client *client, std::vector<char> buffer)
						{
						}

						void SessionController::OnRequest(SystemX::Network::HTTP::Client *client, SystemX::Network::HTTP::Request *request, SystemX::Network::HTTP::Response *response)
						{
							SystemX::Network::HTTP::Controller::OnRequest(client, request, response);
						}
					}
				}
			}
		}
	}
}
