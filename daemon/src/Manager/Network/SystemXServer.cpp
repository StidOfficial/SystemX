#include <SystemX/Daemon/Manager/Network/SystemXServer.hpp>

#include <stdexcept>
#include <thread>
#include <chrono>

#include <SystemX/Logger.hpp> // TEMP

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				SystemXServer::SystemXServer(SystemX::Network::InetAddress address)
					: UDPServer(address)
				{
				}

				/*SystemXServer::SystemXServer(ProtocolFamily family)
					: UDPServer(family)
				{
				}*/

				void SystemXServer::Listen()
				{
					std::thread pending_packets_thread([this] { PendingPackets(); });

					while(!IsShutdown())
					{
						std::vector<char> packetBuffer(1024);
						SystemX::Network::InetAddress addr;

						ssize_t size = RecvFrom(&packetBuffer[0], packetBuffer.size(), addr);
						if(size <= 0)
							continue;

						packet_t packet;
						memcpy(&packet, &packetBuffer[0], sizeof(packet_t));

						if(packet.need_ack)
						{
							packet_t packet_ack;
							packet_ack.packet_id = 0;
							packet_ack.need_ack = false;
							packet_ack.is_ack = true;
							packet_ack.frame_id = packet.frame_id;

							UDPServer::SendTo(reinterpret_cast<char*>(&packet_ack), sizeof(packet_t), addr);
						}
						else if(packet.is_ack)
						{
							m_mutex.lock();
							m_pendingPackets.erase(packet.frame_id);
							m_mutex.unlock();
						}

						if(packet.packet_id == static_cast<int>(Packet::Ping))
							Pong(addr);
					}

					pending_packets_thread.join();
				}

				void SystemXServer::PendingPackets()
				{
					while(!IsShutdown())
					{
						for(auto &packet : m_pendingPackets)
							UDPServer::SendTo(packet.second.data.data(), packet.second.data.size(), packet.second.address);

						std::this_thread::sleep_for(std::chrono::milliseconds(500));
					}
				}

				void SystemXServer::SendTo(uint8_t packetId, bool needAck, const void *buffer, std::size_t length, SystemX::Network::InetAddress address)
				{
					packet_t packet;
					packet.packet_id = packetId;
					packet.need_ack = needAck;
					packet.is_ack = false;
					if(needAck)
						packet.frame_id = m_frames++;
					else
						packet.frame_id = 0;

					std::vector<unsigned char> packetBuffer;
					packetBuffer.insert(packetBuffer.begin(), reinterpret_cast<unsigned char*>(&packet), reinterpret_cast<unsigned char*>(&packet) + sizeof(packet));
					if(buffer)
						packetBuffer.insert(packetBuffer.begin() + sizeof(packet_t), static_cast<const unsigned char*>(buffer), static_cast<const unsigned char*>(buffer) + length);

					if(needAck)
					{
						packet_pending_t pendingPacket;
						pendingPacket.address = address;
						pendingPacket.data = packetBuffer;

						m_mutex.lock();
						m_pendingPackets.emplace(packet.frame_id, pendingPacket);
						m_mutex.unlock();
					}

					UDPServer::SendTo(packetBuffer.data(), packetBuffer.size(), address);
				}

				void SystemXServer::Ping(SystemX::Network::InetAddress address)
				{
					SendTo(static_cast<int>(Packet::Ping), true, nullptr, 0, address);
				}

				void SystemXServer::Pong(SystemX::Network::InetAddress address)
				{
					SendTo(static_cast<int>(Packet::Pong), true, nullptr, 0, address);
				}

				bool SystemXServer::IsShutdown()
				{
					return UDPServer::IsShutdown();
				}

				void SystemXServer::Shutdown()
				{
					UDPServer::Shutdown();
				}

				void SystemXServer::Close()
				{
					UDPServer::Close();
				}
			}
		}
	}
}
