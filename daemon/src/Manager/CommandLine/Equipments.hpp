#ifndef DAEMON_MANAGER_COMMANDLINE_EQUIPMENTS_HPP
#define DAEMON_MANAGER_COMMANDLINE_EQUIPMENTS_HPP

#include <SystemX/Daemon/Manager/CommandLine/Namespace.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				class Equipments : public Namespace
				{
				public:
					Equipments();
					virtual ~Equipments();
				private:
					void Show(Network::TelnetClient *client, std::vector<std::string> args);
					void Remove(Network::TelnetClient *client, std::vector<std::string> args);
					void Add(Network::TelnetClient *client, std::vector<std::string> args);
					void Equipment(Network::TelnetClient *client, std::vector<std::string> args);
					std::string AutoCompleteEquipment(std::vector<std::string> args);
				};
			}
		}
	}
}

#endif
