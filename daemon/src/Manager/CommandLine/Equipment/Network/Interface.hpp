#ifndef DAEMON_MANAGER_COMMANDLINE_EQUIPMENT_NETWORK_INTERFACES_HPP
#define DAEMON_MANAGER_COMMANDLINE_EQUIPMENT_NETWORK_INTERFACES_HPP

#include <SystemX/Daemon/Manager/CommandLine/Namespace.hpp>
#include <SystemX/Equipment/Network/Interface.hpp>
#include <SystemX/Daemon/Manager/Network/TelnetClient.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				namespace Equipment
				{
          namespace Network
          {
  					class Interface : public Namespace
  					{
  					public:
  						Interface(Namespace *base_namespace, SystemX::Equipment::Network::Interface *interface);
  						virtual ~Interface();
  					private:
  					 	SystemX::Equipment::Network::Interface *m_interface;

  						void Name(Manager::Network::TelnetClient *client, std::vector<std::string> args);
							void State(Manager::Network::TelnetClient *client, std::vector<std::string> args);
							void MACAddress(Manager::Network::TelnetClient *client, std::vector<std::string> args);
  						virtual void Exit(Manager::Network::TelnetClient *client, std::vector<std::string> args);
  					};
          }
				}
			}
		}
	}
}

#endif
