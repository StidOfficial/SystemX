#ifndef DAEMON_MANAGER_COMMANDLINE_EQUIPMENT_NETWORK_NETWORK_HPP
#define DAEMON_MANAGER_COMMANDLINE_EQUIPMENT_NETWORK_NETWORK_HPP

#include <SystemX/Daemon/Manager/CommandLine/Namespace.hpp>
#include <SystemX/Daemon/Manager/Equipment/Equipment.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				namespace Equipment
				{
          namespace Network
          {
  					class Network : public Namespace
  					{
  					public:
  						Network(Namespace *base_namespace, Manager::Equipment::Equipment *equipment);
  						virtual ~Network();
  					private:
  						Manager::Equipment::Equipment *m_equipment;

  						void Interfaces(Manager::Network::TelnetClient *client, std::vector<std::string> args);
  						virtual void Exit(Manager::Network::TelnetClient *client, std::vector<std::string> args);
  					};
          }
				}
			}
		}
	}
}

#endif
