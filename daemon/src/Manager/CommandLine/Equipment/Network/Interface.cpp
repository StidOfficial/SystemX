#include "Interface.hpp"

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				namespace Equipment
				{
					namespace Network
					{
						Interface::Interface(Namespace *base_namespace, SystemX::Equipment::Network::Interface *interface)
							: Namespace(base_namespace, interface->GetName()), m_interface(interface)
						{
							RegisterCommand("name", "Show interface name", std::bind(&Interface::Name, this, std::placeholders::_1, std::placeholders::_2));
							RegisterCommand("state", "Show interface state", std::bind(&Interface::State, this, std::placeholders::_1, std::placeholders::_2));
							RegisterCommand("macaddress", "Show interface MAC address", std::bind(&Interface::MACAddress, this, std::placeholders::_1, std::placeholders::_2));
						}

						void Interface::Name(Manager::Network::TelnetClient *client, std::vector<std::string> /*args*/)
						{
							client->Send("\r\nName : " + m_interface->GetName());
						}

						void Interface::State(Manager::Network::TelnetClient *client, std::vector<std::string> args)
						{
							if(m_interface->GetState())
								client->Send("\r\nState : Up");
							else
								client->Send("\r\nState : Down");
						}

						void Interface::MACAddress(Manager::Network::TelnetClient *client, std::vector<std::string> args)
						{
							client->Send("\r\nMAC Address : " + m_interface->GetMACAddress());
						}

						void Interface::Exit(Manager::Network::TelnetClient *client, std::vector<std::string> args)
						{
							Namespace::Exit(client, args);
							delete this;
						}

						Interface::~Interface()
						{
						}
					}
				}
			}
		}
	}
}
