#include "Network.hpp"

#include "Interfaces.hpp"

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				namespace Equipment
				{
					namespace Network
					{
						Network::Network(Namespace *base_namespace, Manager::Equipment::Equipment *equipment)
							: Namespace(base_namespace, "network"), m_equipment(equipment)
						{
							RegisterCommand("interfaces", "Show network interfaces", std::bind(&Network::Interfaces, this, std::placeholders::_1, std::placeholders::_2));
						}

						void Network::Interfaces(Manager::Network::TelnetClient *client, std::vector<std::string> /*args*/)
						{
							CommandLine::Namespace *equipment_namespace = new Equipment::Network::Interfaces(this, m_equipment);
							client->CommandInterface()->ChangeNamespace(equipment_namespace);
						}

						void Network::Exit(Manager::Network::TelnetClient *client, std::vector<std::string> args)
						{
							Namespace::Exit(client, args);
							delete this;
						}

						Network::~Network()
						{
						}
					}
				}
			}
		}
	}
}
