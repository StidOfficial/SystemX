#include "Equipment.hpp"

#include "Network/Network.hpp"
#include "Connectors.hpp"
#include "Drivers.hpp"

#include <SystemX/Daemon/Manager/Manager.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				namespace Equipment
				{
					Equipment::Equipment(Namespace *base_namespace, Daemon::Manager::Equipment::Equipment *equipment)
						: Namespace(base_namespace, equipment->UniqueId()), m_equipment(equipment)
					{
						RegisterCommand("id", "Show id of equipment", std::bind(&Equipment::Id, this, std::placeholders::_1, std::placeholders::_2));
						RegisterCommand("name", "Show or edit hostname", std::bind(&Equipment::Name, this, std::placeholders::_1, std::placeholders::_2));
						RegisterCommand("meminfo", "Show memory information", std::bind(&Equipment::MemInfo, this, std::placeholders::_1, std::placeholders::_2));
						RegisterCommand("power", "Equipment power", std::bind(&Equipment::Power, this, std::placeholders::_1, std::placeholders::_2));
						RegisterCommand("network", "Network configuration", std::bind(&Equipment::Network, this, std::placeholders::_1, std::placeholders::_2));
						RegisterCommand("drivers", "Drivers configuration", std::bind(&Equipment::Drivers, this, std::placeholders::_1, std::placeholders::_2));
						RegisterCommand("connectors", "Connectors configuration", std::bind(&Equipment::Connectors, this, std::placeholders::_1, std::placeholders::_2));
					}

					void Equipment::Id(Daemon::Manager::Network::TelnetClient *client, std::vector<std::string> /*args*/)
					{
						client->Send("\r\n" + m_equipment->UniqueId());
					}

					void Equipment::Name(Daemon::Manager::Network::TelnetClient *client, std::vector<std::string> args)
					{
						if(args.size() > 0)
						{
							Database::DatabaseStatement *statement = Manager::Database()->Prepare("UPDATE equipments SET name = :name WHERE id = :id;");
							statement->Bind(":name", args[0]);
							statement->Bind(":id", m_equipment->Id());
							statement->Execute();
							delete statement;

							m_equipment->SetName(args[0]);
						}
						else
							client->Send("\r\n" + m_equipment->GetName());
					}

					void Equipment::MemInfo(Daemon::Manager::Network::TelnetClient *client, std::vector<std::string> /*args*/)
					{
						client->Send("\r\nMeminfo : " + m_equipment->UniqueId());
					}

					void Equipment::Power(Daemon::Manager::Network::TelnetClient */*client*/, std::vector<std::string> /*args*/)
					{
					}

					void Equipment::Network(Daemon::Manager::Network::TelnetClient *client, std::vector<std::string> /*args*/)
					{
						CommandLine::Namespace *equipment_namespace = new CommandLine::Equipment::Network::Network(this, m_equipment);
						client->CommandInterface()->ChangeNamespace(equipment_namespace);
					}

					void Equipment::Drivers(Daemon::Manager::Network::TelnetClient *client, std::vector<std::string> /*args*/)
					{
						CommandLine::Namespace *equipment_namespace = new CommandLine::Equipment::Drivers(this, m_equipment);
						client->CommandInterface()->ChangeNamespace(equipment_namespace);
					}

					void Equipment::Connectors(Daemon::Manager::Network::TelnetClient *client, std::vector<std::string> /*args*/)
					{
						CommandLine::Namespace *equipment_namespace = new CommandLine::Equipment::Connectors(this, m_equipment);
						client->CommandInterface()->ChangeNamespace(equipment_namespace);
					}

					void Equipment::Exit(Daemon::Manager::Network::TelnetClient *client, std::vector<std::string> args)
					{
						Namespace::Exit(client, args);
						delete this;
					}

					Equipment::~Equipment()
					{
					}
				}
			}
		}
	}
}
