#include "Connectors.hpp"

#include <SystemX/Daemon/Manager/Connectors.hpp>
#include <SystemX/Utils.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				namespace Equipment
				{
					Connectors::Connectors(Namespace *base_namespace, Manager::Equipment::Equipment *equipment)
						: Namespace(base_namespace, "connectors"), m_equipment(equipment)
					{
						RegisterCommand("add", "Add connector for equipment", std::bind(&Connectors::Add, this, std::placeholders::_1, std::placeholders::_2));
						RegisterCommand("remove", "Remove connector for equipment", std::bind(&Connectors::Remove, this, std::placeholders::_1, std::placeholders::_2));
						RegisterCommand("show", "Show connectors used by equipment", std::bind(&Connectors::Show, this, std::placeholders::_1, std::placeholders::_2));
					}

					void Connectors::Add(Network::TelnetClient *client, std::vector<std::string> args)
					{
						if(args.size() > 0)
						{
							Manager::Connector *connector = Manager::Connectors::Get(args[0]);
							if(connector)
								if(!m_equipment->GetConnectors()->Exist(connector))
								{
									std::map<std::string, std::string> params;
									if(args.size() > 1)
										for(auto it = args.begin() + 1; it != args.end(); it++)
										{
											std::vector<std::string> keyValue = Utils::Split(*it, "=");
											params.emplace(keyValue[0], keyValue[1]);
										}

									m_equipment->GetConnectors()->Add(connector, params);
								}
								else
									client->Send("\r\nDriver is already used !");
							else
								client->Send("\r\nDriver name not found !");
						}
						else
							client->Send("\r\nSpecified driver name");
					}

					void Connectors::Remove(Network::TelnetClient *client, std::vector<std::string> args)
					{
						if(args.size() > 0)
						{
							Manager::Connector *connector = Manager::Connectors::Get(args[0]);
							if(connector)
								if(m_equipment->GetConnectors()->Exist(connector))
								{
									m_equipment->GetConnectors()->Remove(connector);
								}
								else
									client->Send("\r\nConnector is not used !");
							else
								client->Send("\r\nConnector name not found !");
						}
						else
							client->Send("\r\nSpecified connector name");
					}

					void Connectors::Show(Network::TelnetClient *client, std::vector<std::string> /*args*/)
					{
						client->Send("\r\nName\t\tVersion");
						client->Send("\r\n--------------------------");

						for(auto &connector : *m_equipment->GetConnectors())
							client->Send("\r\n" + Utils::TableFill(connector->Name(), 8) + Utils::TableFill(connector->Version(), 8));
					}

					void Connectors::Exit(Network::TelnetClient *client, std::vector<std::string> args)
					{
						Namespace::Exit(client, args);
						delete this;
					}

					Connectors::~Connectors()
					{
					}
				}
			}
		}
	}
}
