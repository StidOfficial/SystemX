#include "Drivers.hpp"

#include <SystemX/Drivers.hpp>
#include <SystemX/Daemon/Manager/Manager.hpp>

#include <SystemX/Logger.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace CommandLine
			{
				namespace Equipment
				{
					Drivers::Drivers(Namespace *base_namespace, Daemon::Manager::Equipment::Equipment *equipment)
						: Namespace(base_namespace, "drivers"), m_equipment(equipment)
					{
						RegisterCommand("add", "Add driver for equipment", std::bind(&Drivers::Add, this, std::placeholders::_1, std::placeholders::_2));
						RegisterCommand("remove", "Remove driver for equipment", std::bind(&Drivers::Remove, this, std::placeholders::_1, std::placeholders::_2));
						RegisterCommand("show", "Show drivers used by equipment", std::bind(&Drivers::Show, this, std::placeholders::_1, std::placeholders::_2));
					}

					void Drivers::Add(Network::TelnetClient *client, std::vector<std::string> args)
					{
						if(args.size() > 0)
						{
							SystemX::Driver *driver = SystemX::Drivers::Get(args[0]);
							if(driver)
								if(!m_equipment->Drivers()->Exist(driver))
									m_equipment->Drivers()->Add(driver);
								else
									client->Send("\r\nDriver is already used !");
							else
								client->Send("\r\nDriver name not found !");
						}
						else
							client->Send("\r\nSpecified driver name");
					}

					void Drivers::Remove(Network::TelnetClient *client, std::vector<std::string> args)
					{
						if(args.size() > 0)
						{
							SystemX::Driver *driver = SystemX::Drivers::Get(args[0]);
							if(driver)
								if(m_equipment->Drivers()->Exist(driver))
									m_equipment->Drivers()->Remove(driver);
								else
									client->Send("\r\nDriver is not used !");
							else
								client->Send("\r\nDriver name not found !");
						}
						else
							client->Send("\r\nSpecified driver name");
					}

					void Drivers::Show(Network::TelnetClient *client, std::vector<std::string> /*args*/)
					{
						client->Send("\r\nName\t\tVersion");
						client->Send("\r\n---------------------------");

						for(auto &driver : *m_equipment->Drivers())
							client->Send("\r\n" + SystemX::Utils::TableFill(driver->Name(), 8) + driver->Version());
					}

					void Drivers::Exit(Network::TelnetClient *client, std::vector<std::string> args)
					{
						Namespace::Exit(client, args);
						delete this;
					}

					Drivers::~Drivers()
					{
					}
				}
			}
		}
	}
}
