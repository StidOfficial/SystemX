#include "Client.hpp"

#include <SystemX/Network/UDPServer.hpp>
#include <chrono>
#include <thread>

#include <SystemX/Logger.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Client
		{
			/*SystemX::Network::TCPServer<Daemon::Network::TelnetClient> *Manager::m_telnet_server = nullptr;
			SystemX::Database::Database Manager::m_database(SystemX::Database::Driver::SQLite);*/
			//SystemX::Network::SystemXServer *Daemon::m_systemx_server = nullptr;

			void Client::Initialize()
			{
				SystemX::Logger::Info("Initialize client daemon...");

				/*SystemX::Logger::Info("Initialize database...");
				m_database.Open("systemxd.sqlite3");

				Equipments::Initialize();
				CommandLine::Interface::Register(InitializeInterface);

				std::thread telnet_server_thread(RunTelnetServer);*/

				/*SystemX::Network::UDPServer client(SystemX::Network::ProtocolFamily::IPv4);

				int i = 0;
				while()
				{
					std::vector<char> buffer;
					buffer.resize(255);
					buffer[0] = i;

					SystemX::Network::InetAddress addr(SystemX::Network::AddressFamily::IPv4, "localhost", 6000);
					client.SendTo(&buffer[0], buffer.size(), addr);

					SystemX::Logger::Debug("Send test !");
					//std::this_thread::sleep_for(std::chrono::seconds(1));
					std::this_thread::sleep_for(std::chrono::milliseconds(10));

					i++;
				}

				client.Close();*/

				//telnet_server_thread.join();

				//m_systemx_server = new SystemX::Network::SystemXServer(SystemX::Network::InetAddress(SystemX::Network::IPAddress::IPv4Any, 6000));
			}

			void Client::Run()
			{
				SystemX::Logger::Info("SystemX Server...");

				//m_systemx_server->Listen();

				SystemX::Logger::Info("SystemX Server shutdown !");
			}

			void Client::Shutdown()
			{
				//m_systemx_server->Close();
			}

			void Client::Destroy()
			{
				//delete m_systemx_server;
			}
		}
	}
}
