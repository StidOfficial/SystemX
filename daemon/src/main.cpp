#include "Client/Client.hpp"

#include <SystemX/Daemon/Manager/Manager.hpp>
#include <SystemX/Drivers.hpp>
#include <SystemX/Daemon/Manager/Connectors.hpp>

#include <SystemX/Equipment/LocalEquipment.hpp>
#include <SystemX/Logger.hpp>
#include <SystemX/Plugins.hpp>

// Test
//#include <SystemX/Network/HTTP/HTTPClient.hpp>

#include <cstdlib>
#include <signal.h>

bool is_client = false;
bool is_manager = true;

int exitCode = EXIT_SUCCESS;

void intHandler(int)
{
	if(is_client)
		SystemX::Daemon::Client::Client::Shutdown();
	else if(is_manager)
		SystemX::Daemon::Manager::Manager::Shutdown();
}

int main(int argc , char *argv[])
{
	signal(SIGINT, intHandler);

	/*SystemX::Network::HTTP::HTTPClient client = SystemX::Network::HTTP::HTTPClient();
	SystemX::Network::HTTP::Response *response = client.Post(SystemX::Network::URI("http://10.0.0.254/sysbus/NMC:getWANStatus"));
	SystemX::Logger::Info("Response of request : %d - %s", response->GetStatusCode(), response->GetStatusMessage().c_str());
	SystemX::Logger::Info("String : %s", response->String().c_str());
	delete response;

	SystemX::Network::HTTP::HTTPClient client1 = SystemX::Network::HTTP::HTTPClient();
	//SystemX::Network::HTTP::Response *iptv_response = client1.Post(SystemX::Network::URI("http://10.0.0.254/sysbus/NMC/OrangeTV:getIPTVStatus"));
	SystemX::Network::HTTP::Response *iptv_response = client1.Get(SystemX::Network::URI("http://http.download.nvidia.com/XFree86/Linux-x86_64/latest.txt"));
	SystemX::Logger::Info("Response of request : %d - %s", iptv_response->GetStatusCode(), iptv_response->GetStatusMessage().c_str());
	SystemX::Logger::Info("String : %s", iptv_response->String().c_str());
	delete iptv_response;*/

	for(int i = 0; i < argc; i++)
		if(strcmp(argv[i], "--daemon") == 0)
		{
			is_client = true;
			is_manager = false;
		}
		else if(strcmp(argv[i], "--manager") == 0)
		{
			is_client = false;
			is_manager = true;
		}

	SystemX::Equipment::LocalEquipment::Initialize();

	SystemX::Plugins::Initialize();
	SystemX::Drivers::Initialize();

	if(is_manager)
		SystemX::Daemon::Manager::Connectors::Initialize();

	try
	{
		if(is_client)
		{
			SystemX::Daemon::Client::Client::Initialize();
			SystemX::Daemon::Client::Client::Run();
		}
		else if(is_manager)
		{
			SystemX::Daemon::Manager::Manager::Initialize();
			SystemX::Daemon::Manager::Manager::Run();
		}
	}
	catch(SystemX::Network::SocketException &e)
	{
		SystemX::Logger::Error("Failed: %s", e.what());
		exitCode = EXIT_FAILURE;
	}

	if(is_client)
		SystemX::Daemon::Client::Client::Destroy();
	else if(is_manager)
		SystemX::Daemon::Manager::Manager::Destroy();

	if(is_manager)
		SystemX::Daemon::Manager::Connectors::Destroy();

	SystemX::Drivers::Destroy();
	SystemX::Plugins::Destroy();

	SystemX::Equipment::LocalEquipment::Destroy();

	return exitCode;
}
