BIN_DIR					:= bin

LIB_DIR					:= lib
DAEMON_DIR			:= daemon
DAEMON_LIB_DIR	:= daemon-lib
PLUGINS_DIR			:= plugins

all: bin bin/systemxd.sqlite3
	$(MAKE) -C lib all
	cp $(LIB_DIR)/libsystemx.so $(BIN_DIR)
	$(MAKE) -C daemon-lib SYSTEMX_LIB=../$(BIN_DIR) SYSTEMX_INCLUDE=../$(LIB_DIR)/include all
	cp $(DAEMON_LIB_DIR)/libsystemxd.so $(BIN_DIR)
	$(MAKE) -C daemon SYSTEMX_LIB=../$(BIN_DIR) SYSTEMX_DAEMON_LIB=../$(BIN_DIR) SYSTEMX_INCLUDE=../$(LIB_DIR)/include SYSTEMX_DAEMON_INCLUDE=../$(DAEMON_LIB_DIR)/include all
	cp $(DAEMON_DIR)/systemxd $(BIN_DIR)
	$(MAKE) -C plugins SYSTEMX_LIB=../../$(BIN_DIR) SYSTEMX_DAEMON_LIB=../../$(BIN_DIR) SYSTEMX_INCLUDE=../../$(LIB_DIR)/include SYSTEMX_DAEMON_INCLUDE=../../$(DAEMON_LIB_DIR)/include all
	cp $(PLUGINS_DIR)/livebox/plugin_livebox.so $(BIN_DIR)/plugins

bin/systemxd.sqlite3:
	sqlite3 $(BIN_DIR)/systemxd.sqlite3 < SystemX_Daemon.sql

bin:
	mkdir -p $(BIN_DIR) $(BIN_DIR)/plugins

debug:
	cd $(BIN_DIR) && LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH ./systemxd $(ARGS)

debug-gdb:
	cd $(BIN_DIR) && LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH gdb -ex=run -ex=backtrace -ex=finish -ex=quit --args ./systemxd $(ARGS)

valgrind:
	cd $(BIN_DIR) && LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH valgrind --track-origins=yes --leak-check=full --show-leak-kinds=all ./systemxd $(ARGS)

clean:
	rm -rf $(BIN_DIR)
	$(MAKE) -C lib clean
	$(MAKE) -C daemon-lib clean
	$(MAKE) -C daemon clean
	$(MAKE) -C plugins clean
