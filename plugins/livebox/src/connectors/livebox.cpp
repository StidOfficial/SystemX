#include "livebox.hpp"

#include "sah.hpp"

#include <SystemX/Network/HTTP/Client.hpp>
#include <SystemX/Network/HTTP/Response.hpp>

#include <SystemX/Logger.hpp>

LiveboxConnector::LiveboxConnector()
	: SystemX::Daemon::Manager::Connector("livebox", "1.0.0.2018")
{
}

void LiveboxConnector::Initialize()
{
	SystemX::Logger::Debug("livebox initialized !");
}

void LiveboxConnector::FirstInitialize(SystemX::Daemon::Manager::Equipment::Equipment *equipment, const std::map<std::string, std::string> params)
{
	SystemX::Logger::Debug("livebox first initialized for %s !", equipment->GetName().c_str());
}

void LiveboxConnector::Initialize(SystemX::Daemon::Manager::Equipment::Equipment *equipment, const std::map<std::string, std::string> params)
{
	auto address = params.find("address");
	auto username = params.find("username");
	auto password = params.find("password");

	if(address == params.end() || username == params.end() || password == params.end())
	{
		SystemX::Logger::Debug("Skip API for %s !", equipment->GetName().c_str());

		equipment->SetStatus(false);
		return;
	}

	try
	{
		Sah sah(address->second);
		if(sah.Authenticate(username->second, password->second))
		{
			equipment->SetStatus(true);

			Json::Value root;

			root = sah.DeviceInfo();

			root = sah.IPTVStatus();
			SystemX::Logger::Debug("TV : %s", root["result"]["data"]["IPTVStatus"].asString().c_str());

			root = sah.WANStatus();

			SystemX::Equipment::Network::Interface *wanInterface = new SystemX::Equipment::Network::Interface("wan");
			wanInterface->SetState(root["result"]["data"]["LinkState"] == "up");
			wanInterface->SetMACAddress(root["result"]["data"]["MACAddress"].asString().c_str());

			equipment->Interfaces()->Add(wanInterface);

			root = sah.Dsl();

			SystemX::Equipment::Network::Interface *dsl0Interface = new SystemX::Equipment::Network::Interface("dsl0");
			dsl0Interface->SetState(root["result"]["status"]["dsl"]["dsl0"]["LinkState"] == "Up");

			equipment->Interfaces()->Add(dsl0Interface);
		}
		else
		{
			SystemX::Logger::Error("Authentication API failed for %s !", equipment->GetName().c_str());
			equipment->SetStatus(false);
		}

		/*//response = client.Post(SystemX::Network::URI("http://" + address + "/sysbus/Devices:get"));
		//response = client.Post(SystemX::Network::URI("http://" + address + "/sysbus/UserManagement:getUsers"));*/
	}
	catch(SystemX::Network::SocketException &e)
	{
		SystemX::Logger::Error("Failed livebox : %s", e.what());
		equipment->SetStatus(false);
	}

	SystemX::Logger::Debug("livebox initialized for %s !", equipment->GetName().c_str());
}

void LiveboxConnector::Uninitialize(SystemX::Daemon::Manager::Equipment::Equipment *equipment)
{
	SystemX::Logger::Debug("livebox uninitialized for %s !", equipment->GetName().c_str());
}

void LiveboxConnector::Uninitialize()
{
	SystemX::Logger::Debug("livebox uninitialized !");
}
