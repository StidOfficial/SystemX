#ifndef PLUGIN_LIVEBOX_CONNECTORS_LIVEBOX
#define PLUGIN_LIVEBOX_CONNECTORS_LIVEBOX

#include <SystemX/Daemon/Manager/Connector.hpp>

class LiveboxConnector : public SystemX::Daemon::Manager::Connector
{
public:
	LiveboxConnector();

	virtual void Initialize();
	virtual void FirstInitialize(SystemX::Daemon::Manager::Equipment::Equipment *equipment, const std::map<std::string, std::string> params);
	virtual void Initialize(SystemX::Daemon::Manager::Equipment::Equipment *equipment, const std::map<std::string, std::string> params);
	virtual void Uninitialize(SystemX::Daemon::Manager::Equipment::Equipment *equipment);
	virtual void Uninitialize();
};

#endif
