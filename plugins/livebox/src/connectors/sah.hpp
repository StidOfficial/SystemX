#ifndef PLUGIN_LIVEBOX_CONNECTORS_SAH
#define PLUGIN_LIVEBOX_CONNECTORS_SAH

#include <SystemX/Network/HTTP/Cookies.hpp>
#include <SystemX/Network/HTTP/Request.hpp>

#include <string>
#include <vector>
#include <json/json.h>

class SahException : public std::runtime_error
{
public:
	SahException(const std::string& what_arg)
		: std::runtime_error(what_arg)
	{
	}
};

class Sah
{
public:
	Sah(std::string address);
	~Sah();

	Json::Value Execute(SystemX::Network::HTTP::Method method, std::string path, std::string data);
	Json::Value Execute(SystemX::Network::HTTP::Method method, std::string path, std::vector<char> data = std::vector<char>());
	bool Authenticate(std::string username, std::string password);
	Json::Value Dsl();
	Json::Value WANStatus();
	Json::Value IPTVStatus();
	Json::Value DeviceInfo();
	Json::Value PhoneTrunk();
	Json::Value WifiStats();
private:
	std::string m_address;
	SystemX::Network::HTTP::Cookies *m_cookies;
	std::string m_contextID;
};

#endif
