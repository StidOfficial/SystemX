#include "connectors/livebox.hpp"
#include "drivers/livebox.hpp"

#include <SystemX/Plugins.hpp>
#include <SystemX/Daemon/Manager/Connectors.hpp>
#include <SystemX/Drivers.hpp>
#include <SystemX/Logger.hpp>

class LiveboxPlugin : public SystemX::Plugin
{
public:
	LiveboxPlugin()
		: SystemX::Plugin("livebox", "1.0.0.2018")
	{
		static LiveboxConnector connector;
		SystemX::Daemon::Manager::Connectors::Register(&connector);

		static LiveboxDriver driver;
		SystemX::Drivers::Register(&driver);
	}

	~LiveboxPlugin()
	{
		SystemX::Logger::Debug("Destroy livebox plugin");
	}
};

SYSTEMX_PLUGIN(LiveboxPlugin);
