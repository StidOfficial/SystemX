#ifndef PLUGIN_LIVEBOX_DRIVERS_LIVEBOX
#define PLUGIN_LIVEBOX_DRIVERS_LIVEBOX

#include <SystemX/Driver.hpp>

class LiveboxDriver : public SystemX::Driver
{
public:
	LiveboxDriver();

	virtual void Initialize();
	virtual void Initialize(SystemX::Equipment::Equipment *equipment);
	virtual void Uninitialize(SystemX::Equipment::Equipment *equipment);
	virtual void Uninitialize();
};

#endif
