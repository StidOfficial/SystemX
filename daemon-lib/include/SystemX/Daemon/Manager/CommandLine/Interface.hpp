#ifndef DAEMON_MANAGER_COMMANDLINE_INTERFACE_HPP
#define DAEMON_MANAGER_COMMANDLINE_INTERFACE_HPP

#include "../Network/TelnetClient.hpp"
#include "Namespace.hpp"

#include <functional>
#include <vector>
#include <string>
#include <map>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				class TelnetClient;
			}

			namespace CommandLine
			{
				class Namespace;

				class Interface
				{
				public:
					Interface();
					~Interface();

					void ChangeNamespace(Namespace *name_space);
					Namespace *CurrentNamespace();
					std::string Path();

					Namespace *Base();

					static void Logout(Network::TelnetClient *client, std::vector<std::string> args);
					static void Register(std::function<void(Namespace*)> func);
				private:
					Namespace *m_current;
					Namespace *m_base;
					static std::vector<std::function<void(Namespace*)>> m_initializer;
				};
			}
		}
	}
}

#endif
