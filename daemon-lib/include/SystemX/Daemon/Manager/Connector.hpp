#ifndef DAEMON_MANAGER_CONNECTOR_HPP
#define DAEMON_MANAGER_CONNECTOR_HPP

#include <SystemX/Daemon/Manager/Equipment/Equipment.hpp>

#include <string>
#include <vector>
#include <map>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			class Connector
			{
			public:
				Connector(std::string name, std::string version);
				virtual ~Connector();

				virtual void Initialize();
				virtual void FirstInitialize(Equipment::Equipment *equipment, const std::map<std::string, std::string> params);
				virtual void Initialize(Equipment::Equipment *equipment, const std::map<std::string, std::string> params);
				virtual void Uninitialize(Equipment::Equipment *equipment);
				virtual void Uninitialize();

				void Add(Equipment::Equipment *equipment, const std::map<std::string, std::string> params);
				void Remove(Equipment::Equipment *equipment);

				std::string Name();
				std::string Version();
			private:
				std::string m_name;
				std::string m_version;

				std::vector<Equipment::Equipment*> m_equipments;
			};
		}
	}
}

#endif
