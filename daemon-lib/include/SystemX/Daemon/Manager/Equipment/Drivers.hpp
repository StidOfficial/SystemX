#ifndef DAEMON_MANAGER_EQUIPMENT_DRIVERS_HPP
#define DAEMON_MANAGER_EQUIPMENT_DRIVERS_HPP

#include <SystemX/Equipment/Drivers.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Equipment
			{
				class Drivers : public SystemX::Equipment::Drivers
				{
				public:
					Drivers(SystemX::Equipment::Equipment *equipment);

					void Add(Driver *driver);
					void Remove(Driver *driver);
				};
			}
		}
	}
}

#endif
