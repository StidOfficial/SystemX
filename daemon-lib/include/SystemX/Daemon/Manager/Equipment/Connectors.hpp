#ifndef DAEMON_MANAGER_EQUIPMENT_CONNECTORS_HPP
#define DAEMON_MANAGER_EQUIPMENT_CONNECTORS_HPP

#include <vector>
#include <map>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			class Connector;

			namespace Equipment
			{
				class Equipment;

				class Connectors
				{
				public:
					Connectors(Equipment *equipment);

					bool Exist(Connector *connector);
					//std::string Address(Daemon::Connector *connector);
					void Add(Connector *connector, std::map<std::string, std::string> params);
					void Remove(Connector *connector);

					std::vector<Connector*>::iterator begin();
					std::vector<Connector*>::iterator end();
				protected:
					Equipment* m_equipment;
					std::vector<Connector*> m_connectors;
				};
			}
		}
	}
}

#endif
