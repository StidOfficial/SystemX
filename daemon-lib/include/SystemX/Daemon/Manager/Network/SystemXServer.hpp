#ifndef DAEMON_MANAGER_NETWORK_SYSTEMXSERVER_HPP
#define DAEMON_MANAGER_NETWORK_SYSTEMXSERVER_HPP

#include <SystemX/Network/UDPServer.hpp>

#include <mutex>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Network
			{
				typedef struct packet
				{
					uint8_t packet_id;
					bool need_ack;
					bool is_ack;
					uint64_t frame_id;
				} packet_t;

				typedef struct packet_pending
				{
					SystemX::Network::InetAddress address;
					std::vector<unsigned char> data;
				} packet_pending_t;

				enum Packet
				{
					Ping,
					Pong = Ping
				};

				class SystemXServer : private SystemX::Network::UDPServer
				{
				public:
					SystemXServer(SystemX::Network::InetAddress address);
					//SystemXServer(ProtocolFamily family);

					virtual void Listen();
					void SendTo(uint8_t packetId, bool needAck, const void *buffer, std::size_t length, SystemX::Network::InetAddress address);
					void Ping(SystemX::Network::InetAddress address);
					void Pong(SystemX::Network::InetAddress address);
					virtual bool IsShutdown();
					virtual void Shutdown();
					virtual void Close();
				private:
					int m_frames;
					std::mutex m_mutex;
					std::map<uint16_t, packet_pending_t> m_pendingPackets;

					void PendingPackets();
				};
			}
		}
	}
}

#endif
