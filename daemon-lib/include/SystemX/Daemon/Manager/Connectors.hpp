#ifndef DAEMON_MANAGER_CONNECTORS_HPP
#define DAEMON_MANAGER_CONNECTORS_HPP

#include <SystemX/Daemon/Manager/Connector.hpp>

#include <string>
#include <map>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			class Connectors
			{
			public:
				static void Initialize();
				static void Register(Connector *connector);
				static std::map<std::string, Connector*> All();
				static Connector *Get(std::string name);
				static void Destroy();
			private:
				static std::map<std::string, Connector*> m_connectors;
			};
		}
	}
}

#endif
