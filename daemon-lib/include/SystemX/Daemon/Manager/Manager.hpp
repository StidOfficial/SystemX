#ifndef DAEMON_MANAGER_MANAGER_HPP
#define DAEMON_MANAGER_MANAGER_HPP

#include <SystemX/Daemon/Manager/Network/TelnetClient.hpp>
#include <SystemX/Network/TCPServer.hpp>
#include <SystemX/Database/Database.hpp>
#include <SystemX/Daemon/Manager/Network/SystemXServer.hpp>
#include <SystemX/Network/InetAddress.hpp>
#include <SystemX/Network/HTTP/Server.hpp>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			class Manager
			{
			public:
				static void Initialize();
				static void Run();
				static void InitializeInterface(CommandLine::Namespace *base);
				static SystemX::Database::Database *Database();
				static SystemX::Network::TCPServer<Network::TelnetClient> *TelnetServer();
				static void Shutdown();
				static void Destroy();
			private:
				static SystemX::Network::TCPServer<Network::TelnetClient> *m_telnet_server;
				static Network::SystemXServer *m_systemx_server;
				static SystemX::Network::HTTP::Server *m_http_server;
				static SystemX::Database::Database m_database;

				static void RunTelnetServer();
				static void RunSystemXServer();
				static void RunWebServer();
			};
		}
	}
}

#endif
