#include <SystemX/Daemon/Manager/Equipment/Drivers.hpp>

#include <SystemX/Daemon/Manager/Manager.hpp>
#include <SystemX/Drivers.hpp>

#include <algorithm>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Equipment
			{
				Drivers::Drivers(SystemX::Equipment::Equipment *equipment)
					: SystemX::Equipment::Drivers(equipment)
				{
					Database::DatabaseStatement *statement = Manager::Database()->Prepare("SELECT driver FROM equipments_drivers WHERE equipment = :id;");
					statement->Bind(":id", equipment->Id());
					while(statement->Next())
					{
						Driver *driver = SystemX::Drivers::Get(statement->GetString("driver"));
						if(driver)
							SystemX::Equipment::Drivers::Add(driver);
						else
							Logger::Warn("%s driver is not loaded !", statement->GetString("driver").c_str());
					}

					delete statement;
				}

				void Drivers::Add(Driver *driver)
				{
					SystemX::Equipment::Drivers::Add(driver);

					Database::DatabaseStatement *statement = Manager::Database()->Prepare("INSERT INTO equipments_drivers(equipment, driver) VALUES(:id, :driver);");
					statement->Bind(":id", m_equipment->Id());
					statement->Bind(":driver", driver->Name());
					statement->Execute();

					delete statement;
				}

				void Drivers::Remove(Driver *driver)
				{
					SystemX::Equipment::Drivers::Remove(driver);

					Database::DatabaseStatement *statement = Manager::Database()->Prepare("DELETE FROM equipments_drivers WHERE equipment = :id AND driver = :driver;");
					statement->Bind(":id", m_equipment->Id());
					statement->Bind(":driver", driver->Name());
					statement->Execute();

					delete statement;
				}
			}
		}
	}
}
