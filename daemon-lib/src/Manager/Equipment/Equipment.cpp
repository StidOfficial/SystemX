#include <SystemX/Daemon/Manager/Equipment/Equipment.hpp>

#include <algorithm>

namespace SystemX
{
	namespace Daemon
	{
		namespace Manager
		{
			namespace Equipment
			{
				Equipment::Equipment(int id, std::string uniqueId, std::string name, bool status)
					: SystemX::Equipment::Equipment(
							SystemX::Equipment::Hardware::Hardwares(std::vector<SystemX::Equipment::Hardware::CPU>()),
							std::vector<SystemX::Equipment::OperatingSystem::OperatingSystem*>()
						),
						m_id(id), m_uniqueId(uniqueId), m_name(name), m_status(status),
						m_connectors(std::unique_ptr<Connectors>(new Connectors(this)))
				{
					//m_drivers = std::unique_ptr<Drivers>(new SystemX::Equipment::Drivers(this));
				}

				int Equipment::Id()
				{
					return m_id;
				}

				std::string Equipment::UniqueId()
				{
					return m_uniqueId;
				}

				void Equipment::SetName(std::string name)
				{
					m_name = name;
				}

				std::string Equipment::GetName()
				{
					return m_name;
				}

				void Equipment::SetType(SystemX::Equipment::Type type)
				{
					m_type = type;
				}

				SystemX::Equipment::Type Equipment::GetType()
				{
					return m_type;
				}

				void Equipment::SetDescription(std::string description)
				{
					m_description = description;
				}

				std::string Equipment::GetDescription()
				{
					return m_description;
				}

				void Equipment::SetStatus(bool status)
				{
					m_status = status;
				}

				bool Equipment::GetStatus()
				{
					return m_status;
				}

				Connectors *Equipment::GetConnectors()
				{
					return m_connectors.get();
				}

				Equipment::~Equipment()
				{
				}
			}
		}
	}
}
